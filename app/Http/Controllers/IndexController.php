<?php

namespace App\Http\Controllers;
use App\Models\Contactu;
use App\Http\Requests\Admin\Contactu\StoreContactu;
use App\Models\Blog;
use App\Models\Feedback;
use DB;
use App\Models\Product;
use App\Models\Category;
use Session;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\ImageSetting;
use App\Models\BankDetail;


class IndexController extends Controller
{
    //

   public function __construct()
    {
        $user_id =  Session::get('user_id');
    }

    public function home ()  //home page
    {        
      $blogs = Blog::orderBy('id','Desc')->get();
      $feedbacks = Feedback::where('status','APPROVED')->orderBy('id','Desc')->get(); 
      $newarrival = Product::where('tag','newarrival')->where('status','Active')->get();   
      $catScratchers = Product::where('tag','catscratchers')->where('status','Active')->orderBy('id','Desc')->take(8)->get(); 
      $imagesetting =ImageSetting::where('id',1)->first();
         return view('front.index',compact('blogs','feedbacks','newarrival','catScratchers','imagesetting'));
    }   

    public function aboutus()
    {
      $feedbacks = Feedback::where('status','APPROVED')->orderBy('id','Desc')->get(); 
      $imagesetting =ImageSetting::where('id',1)->first();

    	return view('front.aboutus',compact('feedbacks','imagesetting'));
    }

    public function contactus()
    {
    	return view('front.contactus');
    } 

    public function blogs()
     {
      $blogs = Blog::orderBy('id','Desc')->get();
      return view('front.blogs',compact('blogs'));
     }

    public function singleblog($id)
     {
      $blog = Blog::where('id',$id)->first();
      $blogs = Blog::take(5)->get();

      return view('front.singleblog',compact('blog','blogs'));
     }

   public function products($categoryid,$slug)
   {
     $category = Category::where('id',$categoryid)->first();   
     $products = Product::where('category_id',$categoryid)->get();   
     return view('front.products',compact('products','category'));
   }

   public function produtDetails($id)
   {
     $product = Product::where('id',$id)->first(); 
     $category = Category::where('id',$product->category_id)->first(); 
     return view('front.singleproduct',compact('product','category'));
   }


   static function cartCount(){
    
    //$user_id =  Session::get('user_id');
    $cookie=Session::getId(); 

    return DB::table('cart')->where('cookie',$cookie)->count();
   }

   static function cartItems(){
    
    //$user_id =  Session::get('user_id');

    $cookie=Session::getId(); 

    $cart = DB::table('cart')->where('cookie',$cookie)->get();

    $cartItems = [];
    foreach ($cart as $key=>$value) {     

      $cartItems[$key]['product'] = Product::where('id',$value->product_id)->first();
      $cartItems[$key]['cart_id'] = $value->id;
      $cartItems[$key]['quantity'] = $value->product_quantity;

    }

    return $cartItems;

   }

   public function addtocart(Request $request,$id)
   {   
     
      $cookie=Session::getId(); 
      DB::table('cart')->insert(array('cookie' => $cookie, 'product_id' => $id));

      return back();  
      //return redirect('/login-register');
      //return 0;
       
   }


   static function cartAmount()
   {
      $cookie=Session::getId(); 
    /*$user_id =  Session::get('user_id');*/
     $total = DB::table('cart')
    ->join('products','products.id','cart.product_id')
    ->where('cart.cookie',$cookie)->sum(DB::raw('products.price * cart.product_quantity' ));

    return $total;
   }

 

   public function shopingcart(){
    
    $cartItems = $this->cartItems();    
    $total = $this->cartAmount();
 
    return view('front.shopcart',compact('cartItems','total'));
   }

   public function deleteCartItem($id)
   {
     $cookie=Session::getId(); 
    //$user_id =  Session::get('user_id');
    $cartItems = DB::table('cart')
    ->where('id',$id)
    ->where('cookie',$cookie)
    ->delete();
    return back();
 
   }
   public function checkout()
   {
    
    //$user_id =  Session::get('user_id');
     $cookie=Session::getId(); 

    $total = $this->cartAmount();
    $cartItems = DB::table('cart')
    ->join('products','products.id','cart.product_id')
    ->where('cart.cookie',$cookie)->get();

    //$user_details = Customer::where('id',$user_id)->first();

    $jazzcash = BankDetail::where('id',1)->first();
    $easypaisa = BankDetail::where('id',2)->first();
    $bank = BankDetail::where('id',3)->first();


    return view('front.checkout',compact('total','cartItems','jazzcash','easypaisa','bank'));
  
   }





   public function singlePageAddToCart(Request $request)
   {
     
      $cookie=Session::getId(); 
      DB::table('cart')->insert(
      array('cookie' => $cookie, 'product_id' => $request->product_id,'product_quantity' => $request->quantity));

      return back();  
            

   }


   public function postfeedback(Request $request)
   {

   DB::table('feedback')->insert(
      array('name' => $request->name, 'email' => $request->email,'feedback' => $request->feedback,'phone' =>$request->phone));

   return back()->with('message','Thank you for your feedback, We will review it with in next 24 hours.');


   }

   public function postContact(Request $request)
   {
   
    DB::table('contactus')->insert(
      array('firstname' => $request->firstname, 'email' => $request->email,'message' => $request->message,'phone' =>$request->phone,'lastname'=>$request->lastname));

   return back()->with('message','We will review your query with in next 24 hours.');

   } 
}
