<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SocialLink\BulkDestroySocialLink;
use App\Http\Requests\Admin\SocialLink\DestroySocialLink;
use App\Http\Requests\Admin\SocialLink\IndexSocialLink;
use App\Http\Requests\Admin\SocialLink\StoreSocialLink;
use App\Http\Requests\Admin\SocialLink\UpdateSocialLink;
use App\Models\SocialLink;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SocialLinksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexSocialLink $request
     * @return array|Factory|View
     */
    public function index(IndexSocialLink $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(SocialLink::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'facebook', 'twitter', 'instagram', 'youtube'],

            // set columns to searchIn
            ['id', 'facebook', 'twitter', 'instagram', 'youtube']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.social-link.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.social-link.create');

        return view('admin.social-link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSocialLink $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreSocialLink $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the SocialLink
        $socialLink = SocialLink::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/social-links'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/social-links');
    }

    /**
     * Display the specified resource.
     *
     * @param SocialLink $socialLink
     * @throws AuthorizationException
     * @return void
     */
    public function show(SocialLink $socialLink)
    {
        $this->authorize('admin.social-link.show', $socialLink);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SocialLink $socialLink
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(SocialLink $socialLink)
    {
        $this->authorize('admin.social-link.edit', $socialLink);


        return view('admin.social-link.edit', [
            'socialLink' => $socialLink,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSocialLink $request
     * @param SocialLink $socialLink
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateSocialLink $request, SocialLink $socialLink)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values SocialLink
        $socialLink->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/social-links'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/social-links');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySocialLink $request
     * @param SocialLink $socialLink
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroySocialLink $request, SocialLink $socialLink)
    {
        $socialLink->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroySocialLink $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroySocialLink $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    SocialLink::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
