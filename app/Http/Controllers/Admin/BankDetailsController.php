<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BankDetail\BulkDestroyBankDetail;
use App\Http\Requests\Admin\BankDetail\DestroyBankDetail;
use App\Http\Requests\Admin\BankDetail\IndexBankDetail;
use App\Http\Requests\Admin\BankDetail\StoreBankDetail;
use App\Http\Requests\Admin\BankDetail\UpdateBankDetail;
use App\Models\BankDetail;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class BankDetailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexBankDetail $request
     * @return array|Factory|View
     */
    public function index(IndexBankDetail $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(BankDetail::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'type', 'accounttitle', 'accountnumber'],

            // set columns to searchIn
            ['id', 'type', 'accounttitle', 'accountnumber']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.bank-detail.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.bank-detail.create');

        return view('admin.bank-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBankDetail $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreBankDetail $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the BankDetail
        $bankDetail = BankDetail::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/bank-details'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/bank-details');
    }

    /**
     * Display the specified resource.
     *
     * @param BankDetail $bankDetail
     * @throws AuthorizationException
     * @return void
     */
    public function show(BankDetail $bankDetail)
    {
        $this->authorize('admin.bank-detail.show', $bankDetail);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BankDetail $bankDetail
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(BankDetail $bankDetail)
    {
        $this->authorize('admin.bank-detail.edit', $bankDetail);


        return view('admin.bank-detail.edit', [
            'bankDetail' => $bankDetail,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBankDetail $request
     * @param BankDetail $bankDetail
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateBankDetail $request, BankDetail $bankDetail)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values BankDetail
        $bankDetail->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/bank-details'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/bank-details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBankDetail $request
     * @param BankDetail $bankDetail
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyBankDetail $request, BankDetail $bankDetail)
    {
        $bankDetail->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyBankDetail $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyBankDetail $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    BankDetail::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
