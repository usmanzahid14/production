<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Customer\BulkDestroyCustomer;
use App\Http\Requests\Admin\Customer\DestroyCustomer;
use App\Http\Requests\Admin\Customer\IndexCustomer;
use App\Http\Requests\Admin\Customer\StoreCustomer;
use App\Http\Requests\Admin\Customer\UpdateCustomer;
use App\Models\Customer;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Session;

class CustomersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCustomer $request
     * @return array|Factory|View
     */

     
      public function signin(Request $request)
     {
         
        $customer = Customer::where('email',$request->email)->where('password',$request->password)->first();
        if($customer)
        { 
             /*$customer = Customer::create($request->all());*/
              Session::put('user_id',$customer->id);
             $product_id = Session::get('product_id');
             if($product_id)
             {
                DB::table('cart')->insert(
                array('user_id' => $customer->id, 'product_id' => $product_id)
                );

               return redirect('/');

             }
            else
            {
            return redirect('customers/dashboard');


            }
        }
        else
        {
           return back()->with('Invalid','Invalid Credientials');
          
        }
      }



     public function register(Request $request)
     {
         
        $email = Customer::where('email',$request->email)->first();
        if($email)
        { 

           return back()->with('Already','Email Already Exists');
        }
        else
        {
           $customer = Customer::create($request->all());
           Session::put('user_id',$customer->id);
           return redirect('customers/dashboard');
        }
      
     } 

     public function dashboard()
     {
         $session = Session::get('user_id');
         if($session)
         {
             $customer = Customer::where('id',$session)->first();
             $orders=Db::table('orders')->select('ordernumber')->distinct('ordernumber')->where('user_id',$session)->orderBy('id','DESC')->get();
                foreach ($orders as $key => $value) {
                # code...
                $orderItems[$key] = DB::table('orders') 
                ->where('ordernumber',$value->ordernumber)               
                ->first();
                }
   
  
               return view('front.dashboard',compact('customer','orderItems'));

         }
         else
          {

            return redirect('login-register');
         }

     }

    

    public function index(IndexCustomer $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Customer::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'email', 'phone', 'status'],

            // set columns to searchIn
            ['id', 'name', 'email', 'phone', 'status']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.customer.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.customer.create');

        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCustomer $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCustomer $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Customer
        $customer = Customer::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/customers'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param Customer $customer
     * @throws AuthorizationException
     * @return void
     */
    public function show(Customer $customer)
    {
        $this->authorize('admin.customer.show', $customer);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Customer $customer)
    {
        $this->authorize('admin.customer.edit', $customer);


        return view('admin.customer.edit', [
            'customer' => $customer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCustomer $request
     * @param Customer $customer
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCustomer $request, Customer $customer)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Customer
        $customer->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/customers'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCustomer $request
     * @param Customer $customer
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCustomer $request, Customer $customer)
    {
        $customer->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCustomer $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCustomer $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('customers')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }


     public function logout()
     {
            Session::flush();
            return redirect('/');

     }

     public function updateCustomerProfile(Request $request)
     {
         Customer::where('id',$request->id)
            ->update(['name' => $request->name,
                      'phone' => $request->phone,
                      'address' => $request->address,
                      'city' => $request->city]); 

          return back();             
          
     }
}
