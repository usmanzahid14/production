<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Contactu\BulkDestroyContactu;
use App\Http\Requests\Admin\Contactu\DestroyContactu;
use App\Http\Requests\Admin\Contactu\IndexContactu;
use App\Http\Requests\Admin\Contactu\StoreContactu;
use App\Http\Requests\Admin\Contactu\UpdateContactu;
use App\Models\Contactu;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ContactusController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexContactu $request
     * @return array|Factory|View
     */
    public function index(IndexContactu $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Contactu::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'firstname', 'lastname', 'email', 'phone'],

            // set columns to searchIn
            ['id', 'firstname', 'lastname', 'email', 'phone', 'message']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.contactu.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.contactu.create');

        return view('admin.contactu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreContactu $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreContactu $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Contactu
        $contactu = Contactu::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/contactus'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/contactus');
    }

    /**
     * Display the specified resource.
     *
     * @param Contactu $contactu
     * @throws AuthorizationException
     * @return void
     */
    public function show(Contactu $contactu)
    {
        $this->authorize('admin.contactu.show', $contactu);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Contactu $contactu
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Contactu $contactu)
    {
        $this->authorize('admin.contactu.edit', $contactu);


        return view('admin.contactu.edit', [
            'contactu' => $contactu,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContactu $request
     * @param Contactu $contactu
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateContactu $request, Contactu $contactu)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Contactu
        $contactu->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/contactus'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/contactus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyContactu $request
     * @param Contactu $contactu
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyContactu $request, Contactu $contactu)
    {
        $contactu->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyContactu $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyContactu $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('contactus')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }


   

       
}
