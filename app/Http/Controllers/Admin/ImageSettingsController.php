<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ImageSetting\BulkDestroyImageSetting;
use App\Http\Requests\Admin\ImageSetting\DestroyImageSetting;
use App\Http\Requests\Admin\ImageSetting\IndexImageSetting;
use App\Http\Requests\Admin\ImageSetting\StoreImageSetting;
use App\Http\Requests\Admin\ImageSetting\UpdateImageSetting;
use App\Models\ImageSetting;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ImageSettingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexImageSetting $request
     * @return array|Factory|View
     */
    public function index(IndexImageSetting $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ImageSetting::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7', 'image8', 'image9', 'image10', 'image11', 'image12', 'image13', 'image14', 'image15'],

            // set columns to searchIn
            ['id', 'image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7', 'image8', 'image9', 'image10', 'image11', 'image12', 'image13', 'image14', 'image15']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.image-setting.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.image-setting.create');

        return view('admin.image-setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreImageSetting $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreImageSetting $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the ImageSetting
        $imageSetting = ImageSetting::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/image-settings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/image-settings');
    }

    /**
     * Display the specified resource.
     *
     * @param ImageSetting $imageSetting
     * @throws AuthorizationException
     * @return void
     */
    public function show(ImageSetting $imageSetting)
    {
        $this->authorize('admin.image-setting.show', $imageSetting);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ImageSetting $imageSetting
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ImageSetting $imageSetting)
    {
        $this->authorize('admin.image-setting.edit', $imageSetting);


        return view('admin.image-setting.edit', [
            'imageSetting' => $imageSetting,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateImageSetting $request
     * @param ImageSetting $imageSetting
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateImageSetting $request, ImageSetting $imageSetting)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ImageSetting
        $imageSetting->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/image-settings'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/image-settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyImageSetting $request
     * @param ImageSetting $imageSetting
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyImageSetting $request, ImageSetting $imageSetting)
    {
        $imageSetting->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyImageSetting $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyImageSetting $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ImageSetting::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
