<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\BulkDestroyOrder;
use App\Http\Requests\Admin\Order\DestroyOrder;
use App\Http\Requests\Admin\Order\IndexOrder;
use App\Http\Requests\Admin\Order\StoreOrder;
use App\Http\Requests\Admin\Order\UpdateOrder;
use App\Models\Order;
use App\Models\Product;
use App\Models\Customer;


use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Session;
use App\Mail\OrderMail;
use Mail;


class OrdersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOrder $request
     * @return array|Factory|View
     */
    

    public function placeOrder(Request $request)
    {
      $userAlready = Customer::where('email',$request->email)->first();
      if($userAlready)
      {

        return back()->with('message','Email already in use.');
      }

        $rand = rand(10,5000);
        $ordernumber = $rand.time();
        $cookie=Session::getId(); 

       //dd($ordernumber);
         $cartItems = DB::table('cart')
        ->join('products','products.id','cart.product_id')
        ->where('cart.cookie',$cookie)->get();
         $total_bill = app('App\Http\Controllers\IndexController')->cartAmount();
         //dd($total_bill);
        
        $user_id = Customer::insertGetId([
        'email' => $request->email,
        'password' => $rand,
        'phone' => $request->phone,
        'address' => $request->address,
        'city' => $request->city,
        ]);

        foreach ($cartItems as $key => $value) {
        # code...
            $order[$key] = DB::table('orders')->insert(
            array(
            'ordernumber' => $ordernumber,   
            'user_id' => $user_id,
            'product_id' => $value->product_id,
            'quantity'=>$value->product_quantity,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'notes' => $request->notes,
            'total_bill' => $total_bill,
            'payment_method' => $request->payment_method,
            'bank_receipt' => $request->bank_receipt,

            ));
        }

        $sales = DB::table('sales')->insert(array('ordernumber' => $ordernumber, 'total_bill' => $total_bill));
        $removeFromCart = DB::table('cart')->where('cookie',$cookie)->delete();

        $email = $request->email;
        $order = [
        'ordernumber' => $ordernumber,
        'total_bill' => $total_bill,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'city'=>$request->city,
        'name' => $request->name,
        'payment_method' => $request->payment_method,
        'email' => $request->email,
        'password' => $rand,


        ];
        Mail::to($email)->send(new OrderMail($order));

        
        return redirect('orders/thank-you');
    

    } 

    public function thankyou(){

        return view('front.thankyou');
    }






    public function index(IndexOrder $request)
    {
        $orders  =Db::table('orders')->select('ordernumber')->distinct('ordernumber')->orderBy('id','DESC')->get();
        foreach ($orders as $key => $value) {
        # code...
            $data[$key] = DB::table('orders') 
            ->where('ordernumber',$value->ordernumber)
            ->get();
        }
  
        return view('admin.order.orders', compact('data'));
    }


    public function orderdetails($ordernumber)
    {
         $singleorder = Db::table('orders')->where('ordernumber',$ordernumber)->first();
            $orderItems = DB::table('orders') 
            ->join('products','products.id','orders.product_id')
            ->where('orders.ordernumber',$ordernumber)
            ->get(); 

            $total_bill = DB::table('orders') 
            ->join('products','products.id','orders.product_id')
            ->where('orders.ordernumber',$ordernumber)
            ->sum(DB::raw('products.price * orders.quantity' )); 

 
     return view('admin.order.orderdetails',compact('orderItems','ordernumber','singleorder','total_bill'));
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.order.create');

        return view('admin.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrder $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOrder $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Order
        $order = Order::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/orders'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @throws AuthorizationException
     * @return void
     */
    public function show(Order $order)
    {
        $this->authorize('admin.order.show', $order);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit($id)
    {
        $order = DB::table('orders') 
              ->join('products','products.id','orders.product_id')
             ->where('orders.ordernumber',$id)
             ->select('orders.*','products.product_name','products.price')
            ->get();

        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrder $request
     * @param Order $order
     * @return array|RedirectResponse|Redirector
     */
    public function update(Request $request)
    {
        // Sanitize input
         
        DB::table('orders')->where('ordernumber',$request->ordernumber)->update([
        'status' => $request->status
        ]);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/orders'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOrder $request
     * @param Order $order
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOrder $request, Order $order)
    {
        $order->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOrder $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOrder $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('orders')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
