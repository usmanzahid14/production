<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.product.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => ['required', 'string'],
            'parent_category' => ['nullable', 'string'],
            'sub_category' => ['nullable', 'string'],
            'product_name' => ['nullable', 'string'],
            'price' => ['nullable', 'string'],
            'brand' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'stock_quantity' => ['nullable', 'string'],
            'tag' => ['nullable', 'string'],
            'status' => ['nullable', 'string'],
            'category_name' => ['nullable', 'string'],
            'size' => ['nullable', 'string'],
            'color' => ['nullable', 'string'],
            'firstprice' => ['nullable', 'string'],



            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
