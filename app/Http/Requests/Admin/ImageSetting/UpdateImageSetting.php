<?php

namespace App\Http\Requests\Admin\ImageSetting;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateImageSetting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.image-setting.edit', $this->imageSetting);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'image1' => ['nullable', 'string'],
            'image2' => ['nullable', 'string'],
            'image3' => ['nullable', 'string'],
            'image4' => ['nullable', 'string'],
            'image5' => ['nullable', 'string'],
            'image6' => ['nullable', 'string'],
            'image7' => ['nullable', 'string'],
            'image8' => ['nullable', 'string'],
            'image9' => ['nullable', 'string'],
            'image10' => ['nullable', 'string'],
            'image11' => ['nullable', 'string'],
            'image12' => ['nullable', 'string'],
            'image13' => ['nullable', 'string'],
            'image14' => ['nullable', 'string'],
            'image15' => ['nullable', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
