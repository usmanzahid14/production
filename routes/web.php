<?php

use Illuminate\Support\Facades\Route;/*
use App\Http\Controllers\Admin\RolePermissionController;*/
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 

Route::get('/login-register', function () {
    return view('front.login-register');
});


Route::get('/give-feedback', function () {
    return view('front.give-feedback');
});

 


        Route::namespace('App\Http\Controllers')->group(static function() 
        {
        Route::get('/','IndexController@home');    
        Route::get('/aboutus','IndexController@aboutus');
        Route::get('/contactus','IndexController@contactus');
        Route::post('/send-contactus','IndexController@postContact');
        Route::get('/blogs','IndexController@blogs');
        Route::get('/blog-detail/{id}','IndexController@singleblog');
        Route::get('/products/{categoryid}/{slug}','IndexController@products');
        Route::get('/details/{id}','IndexController@produtDetails');

        Route::get('/addtocart/{id}','IndexController@addtocart');
         Route::get('/shoping-cart','IndexController@shopingcart');
        Route::get('/delete-cart-item/{id}','IndexController@deleteCartItem');
        Route::get('/checkout','IndexController@checkout');
        Route::post('/singlepage-add-to-cart','IndexController@singlePageAddToCart');
        Route::post('/post-feedback','IndexController@postfeedback');




 



        });

 Route::prefix('customers')->namespace('App\Http\Controllers\Admin')->group(static function() 
          {
          Route::post('/register','CustomersController@register');
          Route::get('/dashboard','CustomersController@dashboard');
          Route::get('/logout','CustomersController@logout');
          Route::post('/signin','CustomersController@signin');
          Route::post('/updateCustomerProfile','CustomersController@updateCustomerProfile');
            
        });

    Route::prefix('orders')->namespace('App\Http\Controllers\Admin')->group(static function() 
        {
        Route::post('/place-order','OrdersController@placeOrder');
        Route::get('/thank-you','OrdersController@thankyou');


        });





 

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
             Route::get('/dashboard',                                             'AdminUsersController@dashboard')->name('dashboard');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});


 

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('roles')->name('roles/')->group(static function() {
            Route::get('/',                                             'RolesController@index')->name('index');
            Route::get('/create',                                       'RolesController@create')->name('create');
            Route::post('/',                                            'RolesController@store')->name('store');
            Route::get('/{role}/edit',                                  'RolesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'RolesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{role}',                                      'RolesController@update')->name('update');
            Route::delete('/{role}',                                    'RolesController@destroy')->name('destroy');
        });
    });
});

 

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('customers')->name('customers/')->group(static function() {
            Route::get('/',                                             'CustomersController@index')->name('index');
            Route::get('/create',                                       'CustomersController@create')->name('create');
            Route::post('/',                                            'CustomersController@store')->name('store');
            Route::get('/{customer}/edit',                              'CustomersController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CustomersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{customer}',                                  'CustomersController@update')->name('update');
            Route::delete('/{customer}',                                'CustomersController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('contactus')->name('contactus/')->group(static function() {
            Route::get('/',                                             'ContactusController@index')->name('index');
            Route::get('/create',                                       'ContactusController@create')->name('create');
            Route::post('/',                                            'ContactusController@store')->name('store');
            Route::get('/{contactu}/edit',                              'ContactusController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ContactusController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{contactu}',                                  'ContactusController@update')->name('update');
            Route::delete('/{contactu}',                                'ContactusController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('blogs')->name('blogs/')->group(static function() {
            Route::get('/',                                             'BlogsController@index')->name('index');
            Route::get('/create',                                       'BlogsController@create')->name('create');
            Route::post('/',                                            'BlogsController@store')->name('store');
            Route::get('/{blog}/edit',                                  'BlogsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'BlogsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{blog}',                                      'BlogsController@update')->name('update');
            Route::delete('/{blog}',                                    'BlogsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('feedback')->name('feedback/')->group(static function() {
            Route::get('/',                                             'FeedbackController@index')->name('index');
            Route::get('/create',                                       'FeedbackController@create')->name('create');
            Route::post('/',                                            'FeedbackController@store')->name('store');
            Route::get('/{feedback}/edit',                              'FeedbackController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FeedbackController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{feedback}',                                  'FeedbackController@update')->name('update');
            Route::delete('/{feedback}',                                'FeedbackController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('categories')->name('categories/')->group(static function() {
            Route::get('/',                                             'CategoryController@index')->name('index');
            Route::get('/create',                                       'CategoryController@create')->name('create');
            Route::post('/',                                            'CategoryController@store')->name('store');
            Route::get('/{category}/edit',                              'CategoryController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CategoryController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{category}',                                  'CategoryController@update')->name('update');
            Route::delete('/{category}',                                'CategoryController@destroy')->name('destroy');
             Route::get('/get-from-ajax-request/{parent}/{sub_category}','CategoryController@getCategory')->name('get-from-ajax-request');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('products')->name('products/')->group(static function() {
            Route::get('/',                                             'ProductsController@index')->name('index');
            Route::get('/create',                                       'ProductsController@create')->name('create');
            Route::post('/',                                            'ProductsController@store')->name('store');
            Route::get('/{product}/edit',                               'ProductsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ProductsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{product}',                                   'ProductsController@update')->name('update');
            Route::delete('/{product}',                                 'ProductsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('orders')->name('orders/')->group(static function() {
            Route::get('/',                                             'OrdersController@index')->name('index');
            Route::get('/create',                                       'OrdersController@create')->name('create');
            Route::post('/',                                            'OrdersController@store')->name('store');
            Route::get('/edit/{id}',                                 'OrdersController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'OrdersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/update',                                     'OrdersController@update')->name('update');
            Route::delete('/{order}',                                   'OrdersController@destroy')->name('destroy');

             Route::get('/details/{ordernumber}',                                             'OrdersController@orderdetails')->name('orderdetails');

              
        });


    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('social-links')->name('social-links/')->group(static function() {
            Route::get('/',                                             'SocialLinksController@index')->name('index');
            Route::get('/create',                                       'SocialLinksController@create')->name('create');
            Route::post('/',                                            'SocialLinksController@store')->name('store');
            Route::get('/{socialLink}/edit',                            'SocialLinksController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'SocialLinksController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{socialLink}',                                'SocialLinksController@update')->name('update');
            Route::delete('/{socialLink}',                              'SocialLinksController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('image-settings')->name('image-settings/')->group(static function() {
            Route::get('/',                                             'ImageSettingsController@index')->name('index');
            Route::get('/create',                                       'ImageSettingsController@create')->name('create');
            Route::post('/',                                            'ImageSettingsController@store')->name('store');
            Route::get('/{imageSetting}/edit',                          'ImageSettingsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ImageSettingsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{imageSetting}',                              'ImageSettingsController@update')->name('update');
            Route::delete('/{imageSetting}',                            'ImageSettingsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('bank-details')->name('bank-details/')->group(static function() {
            Route::get('/',                                             'BankDetailsController@index')->name('index');
            Route::get('/create',                                       'BankDetailsController@create')->name('create');
            Route::post('/',                                            'BankDetailsController@store')->name('store');
            Route::get('/{bankDetail}/edit',                            'BankDetailsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'BankDetailsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{bankDetail}',                                'BankDetailsController@update')->name('update');
            Route::delete('/{bankDetail}',                              'BankDetailsController@destroy')->name('destroy');
        });
    });
});