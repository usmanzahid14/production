<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'last_login_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Role::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'guard_name' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Permission::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'guard_name' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\GeoVar::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\GeoVar::class, static function (Faker\Generator $faker) {
    return [
        'datetime' => $faker->sentence,
        'comments' => $faker->sentence,
        'name' => $faker->firstName,
        'description' => $faker->sentence,
        'lat1' => $faker->sentence,
        'lng1' => $faker->sentence,
        'lat2' => $faker->sentence,
        'lng2' => $faker->sentence,
        'tpl_var' => $faker->sentence,
        'tpl_val' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Post::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'perex' => $faker->text(),
        'published_at' => $faker->date(),
        'enabled' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'email' => $faker->email,
        'email_verified_at' => $faker->dateTime,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Customer::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'phone' => $faker->sentence,
        'status' => $faker->sentence,
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ContactU::class, static function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->sentence,
        'lastname' => $faker->sentence,
        'email' => $faker->email,
        'phone' => $faker->sentence,
        'message' => $faker->text(),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Contactu::class, static function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->sentence,
        'lastname' => $faker->sentence,
        'email' => $faker->email,
        'phone' => $faker->sentence,
        'message' => $faker->text(),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Blog::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->text(),
        'tag' => $faker->sentence,
        'category' => $faker->sentence,
        'blogimage' => $faker->sentence,
        'authorimage' => $faker->sentence,
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Feedback::class, static function (Faker\Generator $faker) {
    return [
        'feedback' => $faker->text(),
        'name' => $faker->firstName,
        'userimage' => $faker->sentence,
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Category::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'slug' => $faker->unique()->slug,
        'parent' => $faker->sentence,
        'subcategory' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Product::class, static function (Faker\Generator $faker) {
    return [
        'category_id' => $faker->randomNumber(5),
        'parent_category' => $faker->sentence,
        'sub_category' => $faker->sentence,
        'product_name' => $faker->sentence,
        'price' => $faker->sentence,
        'brand' => $faker->sentence,
        'description' => $faker->text(),
        'stock_quantity' => $faker->sentence,
        'tag' => $faker->sentence,
        'status' => $faker->sentence,
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Order::class, static function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomNumber(5),
        'product_id' => $faker->sentence,
        'name' => $faker->firstName,
        'email' => $faker->email,
        'phone' => $faker->sentence,
        'address' => $faker->sentence,
        'city' => $faker->sentence,
        'payment_method' => $faker->sentence,
        'status' => $faker->sentence,
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\SocialLink::class, static function (Faker\Generator $faker) {
    return [
        'facebook' => $faker->sentence,
        'twitter' => $faker->sentence,
        'instagram' => $faker->sentence,
        'youtube' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ImageSetting::class, static function (Faker\Generator $faker) {
    return [
        'image1' => $faker->sentence,
        'image2' => $faker->sentence,
        'image3' => $faker->sentence,
        'image4' => $faker->sentence,
        'image5' => $faker->sentence,
        'image6' => $faker->sentence,
        'image7' => $faker->sentence,
        'image8' => $faker->sentence,
        'image9' => $faker->sentence,
        'image10' => $faker->sentence,
        'image11' => $faker->sentence,
        'image12' => $faker->sentence,
        'image13' => $faker->sentence,
        'image14' => $faker->sentence,
        'image15' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\BankDetail::class, static function (Faker\Generator $faker) {
    return [
        'type' => $faker->sentence,
        'accounttitle' => $faker->sentence,
        'accountnumber' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
