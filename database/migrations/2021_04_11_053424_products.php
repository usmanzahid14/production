<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products', function (Blueprint $table) {
        $table->id();
        $table->integer('category_id');
        $table->string('parent_category')->nullable();
        $table->string('sub_category')->nullable();
        $table->string('product_name')->nullable();
        $table->string('price')->nullable();
        $table->string('brand')->nullable();
        $table->text('description')->nullable();
        $table->string('stock_quantity')->nullable();
        $table->string('tag')->nullable();
        $table->string('status')->nullable();
        $table->softDeletes();
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

         Schema::dropIfExists('products');
        
    }
}
