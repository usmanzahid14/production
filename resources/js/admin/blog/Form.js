import AppForm from '../app-components/Form/AppForm';

Vue.component('blog-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                description:  '' ,
                tag:  '' ,
                category:  '' ,
                blogimage:  '' ,
                authorimage:  '' ,
                
            }
        }
    }

});