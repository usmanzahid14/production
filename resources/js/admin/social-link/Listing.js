import AppListing from '../app-components/Listing/AppListing';

Vue.component('social-link-listing', {
    mixins: [AppListing]
});