import AppForm from '../app-components/Form/AppForm';

Vue.component('social-link-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                facebook:  '' ,
                twitter:  '' ,
                instagram:  '' ,
                youtube:  '' ,
                
            }
        }
    }

});