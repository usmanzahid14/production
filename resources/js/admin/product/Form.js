import AppForm from '../app-components/Form/AppForm';

Vue.component('product-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                category_id:  '' ,
                parent_category:  '' ,
                sub_category:  '' ,
                product_name:  '' ,
                price:  '' ,
                brand:  '' ,
                description:  '' ,
                stock_quantity:  '' ,
                tag:  '' ,
                status:  '' ,
                reset:  '' ,
                
            },
            mediaCollections: ['product_image']
        }
    }

});