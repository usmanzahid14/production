import AppListing from '../app-components/Listing/AppListing';

Vue.component('bank-detail-listing', {
    mixins: [AppListing]
});