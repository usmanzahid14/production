import AppForm from '../app-components/Form/AppForm';

Vue.component('image-setting-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                image1:  '' ,
                image2:  '' ,
                image3:  '' ,
                image4:  '' ,
                image5:  '' ,
                image6:  '' ,
                image7:  '' ,
                image8:  '' ,
                image9:  '' ,
                image10:  '' ,
                image11:  '' ,
                image12:  '' ,
                image13:  '' ,
                image14:  '' ,
                image15:  '' ,
                
            }
        }
    }

});