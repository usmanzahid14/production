import AppListing from '../app-components/Listing/AppListing';

Vue.component('image-setting-listing', {
    mixins: [AppListing]
});