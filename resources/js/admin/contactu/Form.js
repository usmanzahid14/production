import AppForm from '../app-components/Form/AppForm';

Vue.component('contactu-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                firstname:  '' ,
                lastname:  '' ,
                email:  '' ,
                phone:  '' ,
                message:  '' ,
                
            }
        }
    }

});