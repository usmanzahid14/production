import AppForm from '../app-components/Form/AppForm';

Vue.component('order-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                user_id:  '' ,
                product_id:  '' ,
                name:  '' ,
                email:  '' ,
                phone:  '' ,
                address:  '' ,
                city:  '' ,
                payment_method:  '' ,
                status:  '' ,
                
            }
        }
    }

});