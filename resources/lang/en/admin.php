<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'role' => [
        'title' => 'Roles',

        'actions' => [
            'index' => 'Roles',
            'create' => 'New Role',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'guard_name' => 'Guard name',
            
        ],
    ],


     'customer' => [
        'title' => 'Customers',

        'actions' => [
            'index' => 'Customers',
            'create' => 'New Customer',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'password' => 'Password',
            'status' => 'Status',

            
        ],
    ],

    'contactu' => [
        'title' => 'Contact Us',

        'actions' => [
            'index' => 'Contact Us',
            'create' => 'New Contact Us',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
             'email' => 'Email',
            'phone' => 'Phone',           
            'mesaage' => 'Message',

            
        ],
    ],

    'blog' => [
        'title' => 'Blogs',

        'actions' => [
            'index' => 'Blogs',
            'create' => 'New Blog',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Ttile',
            'authorname' => 'Author Name',
             'description' => 'Description',
            'category' => 'Category',           
            'tag' => 'Tag',
            'authorimage' => 'Author Image',
            'blogimage' => 'Blog Image',


            
        ],
    ],
    'feedback' => [
        'title' => 'Feedback',

        'actions' => [
            'index' => 'Feedback',
            'create' => 'New Feedback',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'feedback' => 'Feedback',
            'status' => 'Status',
            'name' => 'User Name',
            'phone' => 'Phone',
            'email' => 'Email',



            


            
        ],
    ],

      'category' => [
        'title' => 'Categories',

        'actions' => [
            'index' => 'Categories',
            'create' => 'New Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'parent' => 'Parent Category',
            'slug' => 'Slug',
            'subcategory' => 'Sub Category',        


            
        ],
    ],

    'order' => [
        'title' => 'Order',

        'actions' => [
            'index' => 'Orders',
            'create' => 'New Order',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'address' => 'Address',
            'city' => 'City', 
            'status' => 'Status',        
            'payment_method' => 'Payment Method',  
            'notes' => 'Extra Notes',     
            'email' => 'Email',        





            
        ],
    ],

    'product' => [
        'title' => 'Products',

        'actions' => [
            'index' => 'Products',
            'create' => 'New Product',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'parent_category' => 'Parent Category',
            'sub_category' => 'Sub Category',
            'price' => 'Price',
            'description' => 'Description',        
            'stock_quantity' => 'Stock Quantity',        
            'tag' => 'Tag',        
            'brand' => 'Brand',
            'category_id' => 'Category',
            'status' => 'Status',
            'category_name' => 'Category',
            'size' => 'Size',
            'color' => 'Color',            
        ],
    ],

    'social-link' => [
        'title' => 'Social Links',

        'actions' => [
            'index' => 'Social Links',
            'create' => 'New Social Link',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'youtube' => 'You Tube',
        ],
    ],

    'image-setting' => [
        'title' => 'Image Settings',

        'actions' => [
            'index' => 'Image Settings',
            'create' => 'New Image Settings',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'image1' => 'Home Page Slider Image 1',
            'image2' => 'Home Page Slider Image 2',
            'image3' => 'Home Page Image 3',
            'image4' => 'Home Page Image 4',
            'image5' => 'Home Page Image 5',
            'image6' => 'Home Page Image 6',
            'image7' => 'Youtube Videp Link 7',
            'image8' => 'Footer image 8',
            'image9' => 'Home Page image 9',
            'image10' => 'Home Page Footer image 10',
            'image11' => 'About us 1 Image 11',
            'image12' => 'About us 2 Image12',
            'image13' => 'About us 3 Image13',
            'image14' => 'About us 4 Image14',
            'image15' => 'About us 5 Image15',

           
        ],
    ],

    'bank-detail' => [
        'title' => 'Bank Details',

        'actions' => [
            'index' => 'Bank Details',
            'create' => 'New Bank Details',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'type' => 'Type',
            'accounttitle' => 'Account Title',
            'accountnumber' => 'Account Number',
        ],
    ],



   

    // Do not delete me :) I'm used for auto-generation
];