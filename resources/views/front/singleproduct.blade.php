@include('front.header')

<title>{{@$product->product_name}}</title>
<main id="body-content" style="margin-top: 20%;">

        <!-- Shopping Wide Start -->
        <section class="wide-tb-100 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="product-gallery">
                            <div class="outer">
                                <div id="big" class="owl-carousel owl-theme">
                                @foreach ($product->getMedia('product_image') as $image)
                                    <div class="item">
                                       <img src="{{ ($image->getUrl()) }}" alt="">
                                    </div>
                                 @endforeach
                                    
                                </div>
                                <div id="thumbs" class="owl-carousel owl-theme">
                                	 @foreach ($product->getMedia('product_image') as $image)
                                    <div class="item" >
                                        <img src="{{ ($image->getUrl()) }}" alt="petpat.pk" >
                                    </div>
                                 @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-description">
                            <h2 class="title">{{@$product->product_name}}</h2>
                            <div class="price">{{@$product->price}}Rs</div>
                            <!-- <p>The Pop Up Bowl was designed for convenience. Made of silicone, its collapsible design allows it to be flattened and extended with ease. This makes it ideal to carry with you on holiday or to the dog park! </p> -->

                           <!--  <div class="collapse" id="collapseExample">
                                <div>
                                  <p>Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.</p>
                                </div>
                            </div>

                            <a class="link-oragne" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Read More</a> -->

                            <div class="product-highlight">
                                @if($product->size)
                                <h5><strong>Size</strong></h5>
                                 <p>{{@$product->size}}</p>
                                 @endif
                                 @if($product->color)
                                 <h5><strong>Color</strong></h5>
                                 <p>{{@$product->color}}</p>
                                 @endif
                                 
                                 @if($product->brand)
                                 <h5><strong>Brand</strong></h5>
                                 <p>{{@$product->brand}}</p>
                                 @endif
                                 

                                 

                                <h5><strong>Category</strong></h5>
                                <p>{{@$category->name}}</p>

                            </div>
                           <form action="{{url('/singlepage-add-to-cart')}}" method="post">
                            @csrf
                            <input type="hidden" name="product_id" value="{{@$product->id}}">
                            <div class="quantity">
                                <button class="minus-btn" type="button" name="button">
                                    <i data-feather="minus"></i>
                                </button>                                        
                                <input type="text" name="quantity" value="0">
                                <button class="plus-btn" type="button" name="button">
                                    <i data-feather="plus"></i>
                                </button>
                            </div>


                            <button class="btn-theme bg-green btn-shadow ml-4" type="submit">ADD TO CART</button>
                             </form>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row mt-4">
                    <div class="col-md-12 col-lg-10 mx-auto">
                        <div class="review-tabbing">
                            <ul class="nav nav-pills theme-tabbing mb-3 justify-content-center" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                        role="tab" aria-controls="pills-home" aria-selected="true">Description</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                        role="tab" aria-controls="pills-profile" aria-selected="false">Additional information</a>
                                </li> -->
                            </ul>
                            <div class="tab-content theme-tabbing" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <p><?php echo html_entity_decode($product->description);?></p>

                                    
                                </div>
                                <!-- <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                    aria-labelledby="pills-profile-tab">
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0">
                                            <tbody>
                                                <tr>
                                                    <th class="text-extra-dark-gray font-weight-500">Brand</th>
                                                    <td>{{@$row->brand}}</td>
                                                </tr>
                                                <tr class="bg-light-gray">
                                                    <th class="text-extra-dark-gray font-weight-500">Size</th>
                                                    <td>{{@$product->size}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="text-extra-dark-gray font-weight-500">Color</th>
                                                    <td>{{@$product->color}}</td>
                                                </tr>
                                                 
                                            </tbody>
                                        </table>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Shopping Wide End -->

    </main>

 <!-- Search Popup Start -->
    <div class="overlay overlay-hugeinc">
        <form class="form-inline mt-2 mt-md-0">
            <div class="form-inner">
                <div class="form-inner-div d-inline-flex align-items-center no-gutters">
                    <div class="col-md-1">
                        <i class="icofont-search"></i>
                    </div>
                    <div class="col-10">
                        <input class="form-control w-100 p-0" type="text" placeholder="Search" aria-label="Search">
                    </div>
                    <div class="col-md-1">
                        <a href="#" class="overlay-close link-oragne"><i class="icofont-close-line"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Search Popup End -->

    

    <!-- Jquery Library JS -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/theme-plugins.min.js')}}"></script>    
    <!-- Theme Custom FIle -->
    <script src="{{url('js/site-custom.js')}}"></script>
    <!-- Page Level Js -->
    <script src="{{url('js/product-slider.js')}}"></script>    
@include('front.footer')

