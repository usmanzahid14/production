@include('front.header')

 <!-- Page Breadcrumbs Start -->
    <section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page">
            <img class="ptt-png" src="{{url('/images/Dot-Shape.png')}}" alt="png">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                    </ol>
                 </nav>
                <h1>Checkout</h1>
            </div>
        </div>
    </section>
    <!-- Page Breadcrumbs End -->

    <!-- Main Body Content Start -->
    <main id="body-content">

        <!-- Billing Details Start -->
       
        <section class="wide-tb-100 pb-0">
        	 @if(count($cartItems) > 0)
            <div class="container">

				@if(session('message'))
				<p class="alert alert-danger">
				{{session('message')}}</p>
				@endif
                 
                <form method="post" action="{{url('/orders/place-order')}}">
                	@csrf

                <div class="row">
                    <div class="col-md-7 mb-0">
                        <div class="checkout-details">
                            <h2 class="fw-7 txt-blue">Billing Details</h2>
                            <div class="row">
                                <div class="col-md-12 mb-0">
                                    <div class="form-group">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" id="name" class="form-control" required="">
                                    </div>
                                </div>


                                
                                <div class="col-md-12 mb-0">
                                    <div class="form-group">
                                        <label for="Street_address">Address <span class="text-danger">*</span></label>
                                        <input type="text" name="address" id="email" class="form-control mb-3" >
                                        
                                    </div>
                                </div>
 

                                <div class="col-md-12 mb-0">
                                    <div class="form-group">
                                        <label for="city">City <span class="text-danger">*</span></label>
                                        <select class="form-control wide small-h" name="city">
											<option value="" disabled selected>Select The City</option>
											<option value="Islamabad">Islamabad</option>
											<option value="" disabled>Punjab Cities</option>
											<option value="Ahmed Nager Chatha">Ahmed Nager Chatha</option>
											<option value="Ahmadpur East">Ahmadpur East</option>
											<option value="Ali Khan Abad">Ali Khan Abad</option>
											<option value="Alipur">Alipur</option>
											<option value="Arifwala">Arifwala</option>
											<option value="Attock">Attock</option>
											<option value="Bhera">Bhera</option>
											<option value="Bhalwal">Bhalwal</option>
											<option value="Bahawalnagar">Bahawalnagar</option>
											<option value="Bahawalpur">Bahawalpur</option>
											<option value="Bhakkar">Bhakkar</option>
											<option value="Burewala">Burewala</option>
											<option value="Chillianwala">Chillianwala</option>
											<option value="Chakwal">Chakwal</option>
											<option value="Chichawatni">Chichawatni</option>
											<option value="Chiniot">Chiniot</option>
											<option value="Chishtian">Chishtian</option>
											<option value="Daska">Daska</option>
											<option value="Darya Khan">Darya Khan</option>
											<option value="Dera Ghazi Khan">Dera Ghazi Khan</option>
											<option value="Dhaular">Dhaular</option>
											<option value="Dina">Dina</option>
											<option value="Dinga">Dinga</option>
											<option value="Dipalpur">Dipalpur</option>
											<option value="Faisalabad">Faisalabad</option>
											<option value="Ferozewala">Ferozewala</option>
											<option value="Fateh Jhang">Fateh Jang</option>
											<option value="Ghakhar Mandi">Ghakhar Mandi</option>
											<option value="Gojra">Gojra</option>
											<option value="Gujranwala">Gujranwala</option>
											<option value="Gujrat">Gujrat</option>
											<option value="Gujar Khan">Gujar Khan</option>
											<option value="Hafizabad">Hafizabad</option>
											<option value="Haroonabad">Haroonabad</option>
											<option value="Hasilpur">Hasilpur</option>
											<option value="Haveli Lakha">Haveli Lakha</option>
											<option value="Jatoi">Jatoi</option>
											<option value="Jalalpur">Jalalpur</option>
											<option value="Jattan">Jattan</option>
											<option value="Jampur">Jampur</option>
											<option value="Jaranwala">Jaranwala</option>
											<option value="Jhang">Jhang</option>
											<option value="Jhelum">Jhelum</option>
											<option value="Kalabagh">Kalabagh</option>
											<option value="Karor Lal Esan">Karor Lal Esan</option>
											<option value="Kasur">Kasur</option>
											<option value="Kamalia">Kamalia</option>
											<option value="Kamoke">Kamoke</option>
											<option value="Khanewal">Khanewal</option>
											<option value="Khanpur">Khanpur</option>
											<option value="Kharian">Kharian</option>
											<option value="Khushab">Khushab</option>
											<option value="Kot Addu">Kot Addu</option>
											<option value="Jauharabad">Jauharabad</option>
											<option value="Lahore">Lahore</option>
											<option value="Lalamusa">Lalamusa</option>
											<option value="Layyah">Layyah</option>
											<option value="Liaquat Pur">Liaquat Pur</option>
											<option value="Lodhran">Lodhran</option>
											<option value="Malakwal">Malakwal</option>
											<option value="Mamoori">Mamoori</option>
											<option value="Mailsi">Mailsi</option>
											<option value="Mandi Bahauddin">Mandi Bahauddin</option>
											<option value="Mian Channu">Mian Channu</option>
											<option value="Mianwali">Mianwali</option>
											<option value="Multan">Multan</option>
											<option value="Murree">Murree</option>
											<option value="Muridke">Muridke</option>
											<option value="Mianwali Bangla">Mianwali Bangla</option>
											<option value="Muzaffargarh">Muzaffargarh</option>
											<option value="Narowal">Narowal</option>
											<option value="Nankana Sahib">Nankana Sahib</option>
											<option value="Okara">Okara</option>
											<option value="Renala Khurd">Renala Khurd</option>
											<option value="Pakpattan">Pakpattan</option>
											<option value="Pattoki">Pattoki</option>
											<option value="Pir Mahal">Pir Mahal</option>
											<option value="Qaimpur">Qaimpur</option>
											<option value="Qila Didar Singh">Qila Didar Singh</option>
											<option value="Rabwah">Rabwah</option>
											<option value="Raiwind">Raiwind</option>
											<option value="Rajanpur">Rajanpur</option>
											<option value="Rahim Yar Khan">Rahim Yar Khan</option>
											<option value="Rawalpindi">Rawalpindi</option>
											<option value="Sadiqabad">Sadiqabad</option>
											<option value="Safdarabad">Safdarabad</option>
											<option value="Sahiwal">Sahiwal</option>
											<option value="Sangla Hill">Sangla Hill</option>
											<option value="Sarai Alamgir">Sarai Alamgir</option>
											<option value="Sargodha">Sargodha</option>
											<option value="Shakargarh">Shakargarh</option>
											<option value="Sheikhupura">Sheikhupura</option>
											<option value="Sialkot">Sialkot</option>
											<option value="Sohawa">Sohawa</option>
											<option value="Soianwala">Soianwala</option>
											<option value="Siranwali">Siranwali</option>
											<option value="Talagang">Talagang</option>
											<option value="Taxila">Taxila</option>
											<option value="Toba Tek Singh">Toba Tek Singh</option>
											<option value="Vehari">Vehari</option>
											<option value="Wah Cantonment">Wah Cantonment</option>
											<option value="Wazirabad">Wazirabad</option>
											<option value="" disabled>Sindh Cities</option>
											<option value="Badin">Badin</option>
											<option value="Bhirkan">Bhirkan</option>
											<option value="Rajo Khanani">Rajo Khanani</option>
											<option value="Chak">Chak</option>
											<option value="Dadu">Dadu</option>
											<option value="Digri">Digri</option>
											<option value="Diplo">Diplo</option>
											<option value="Dokri">Dokri</option>
											<option value="Ghotki">Ghotki</option>
											<option value="Haala">Haala</option>
											<option value="Hyderabad">Hyderabad</option>
											<option value="Islamkot">Islamkot</option>
											<option value="Jacobabad">Jacobabad</option>
											<option value="Jamshoro">Jamshoro</option>
											<option value="Jungshahi">Jungshahi</option>
											<option value="Kandhkot">Kandhkot</option>
											<option value="Kandiaro">Kandiaro</option>
											<option value="Karachi">Karachi</option>
											<option value="Kashmore">Kashmore</option>
											<option value="Keti Bandar">Keti Bandar</option>
											<option value="Khairpur">Khairpur</option>
											<option value="Kotri">Kotri</option>
											<option value="Larkana">Larkana</option>
											<option value="Matiari">Matiari</option>
											<option value="Mehar">Mehar</option>
											<option value="Mirpur Khas">Mirpur Khas</option>
											<option value="Mithani">Mithani</option>
											<option value="Mithi">Mithi</option>
											<option value="Mehrabpur">Mehrabpur</option>
											<option value="Moro">Moro</option>
											<option value="Nagarparkar">Nagarparkar</option>
											<option value="Naudero">Naudero</option>
											<option value="Naushahro Feroze">Naushahro Feroze</option>
											<option value="Naushara">Naushara</option>
											<option value="Nawabshah">Nawabshah</option>
											<option value="Nazimabad">Nazimabad</option>
											<option value="Qambar">Qambar</option>
											<option value="Qasimabad">Qasimabad</option>
											<option value="Ranipur">Ranipur</option>
											<option value="Ratodero">Ratodero</option>
											<option value="Rohri">Rohri</option>
											<option value="Sakrand">Sakrand</option>
											<option value="Sanghar">Sanghar</option>
											<option value="Shahbandar">Shahbandar</option>
											<option value="Shahdadkot">Shahdadkot</option>
											<option value="Shahdadpur">Shahdadpur</option>
											<option value="Shahpur Chakar">Shahpur Chakar</option>
											<option value="Shikarpaur">Shikarpaur</option>
											<option value="Sukkur">Sukkur</option>
											<option value="Tangwani">Tangwani</option>
											<option value="Tando Adam Khan">Tando Adam Khan</option>
											<option value="Tando Allahyar">Tando Allahyar</option>
											<option value="Tando Muhammad Khan">Tando Muhammad Khan</option>
											<option value="Thatta">Thatta</option>
											<option value="Umerkot">Umerkot</option>
											<option value="Warah">Warah</option>
											<option value="" disabled>Khyber Cities</option>
											<option value="Abbottabad">Abbottabad</option>
											<option value="Adezai">Adezai</option>
											<option value="Alpuri">Alpuri</option>
											<option value="Akora Khattak">Akora Khattak</option>
											<option value="Ayubia">Ayubia</option>
											<option value="Banda Daud Shah">Banda Daud Shah</option>
											<option value="Bannu">Bannu</option>
											<option value="Batkhela">Batkhela</option>
											<option value="Battagram">Battagram</option>
											<option value="Birote">Birote</option>
											<option value="Chakdara">Chakdara</option>
											<option value="Charsadda">Charsadda</option>
											<option value="Chitral">Chitral</option>
											<option value="Daggar">Daggar</option>
											<option value="Dargai">Dargai</option>
											<option value="Darya Khan">Darya Khan</option>
											<option value="Dera Ismail Khan">Dera Ismail Khan</option>
											<option value="Doaba">Doaba</option>
											<option value="Dir">Dir</option>
											<option value="Drosh">Drosh</option>
											<option value="Hangu">Hangu</option>
											<option value="Haripur">Haripur</option>
											<option value="Karak">Karak</option>
											<option value="Kohat">Kohat</option>
											<option value="Kulachi">Kulachi</option>
											<option value="Lakki Marwat">Lakki Marwat</option>
											<option value="Latamber">Latamber</option>
											<option value="Madyan">Madyan</option>
											<option value="Mansehra">Mansehra</option>
											<option value="Mardan">Mardan</option>
											<option value="Mastuj">Mastuj</option>
											<option value="Mingora">Mingora</option>
											<option value="Nowshera">Nowshera</option>
											<option value="Paharpur">Paharpur</option>
											<option value="Pabbi">Pabbi</option>
											<option value="Peshawar">Peshawar</option>
											<option value="Saidu Sharif">Saidu Sharif</option>
											<option value="Shorkot">Shorkot</option>
											<option value="Shewa Adda">Shewa Adda</option>
											<option value="Swabi">Swabi</option>
											<option value="Swat">Swat</option>
											<option value="Tangi">Tangi</option>
											<option value="Tank">Tank</option>
											<option value="Thall">Thall</option>
											<option value="Timergara">Timergara</option>
											<option value="Tordher">Tordher</option>
											<option value="" disabled>Balochistan Cities</option>
											<option value="Awaran">Awaran</option>
											<option value="Barkhan">Barkhan</option>
											<option value="Chagai">Chagai</option>
											<option value="Dera Bugti">Dera Bugti</option>
											<option value="Gwadar">Gwadar</option>
											<option value="Harnai">Harnai</option>
											<option value="Jafarabad">Jafarabad</option>
											<option value="Jhal Magsi">Jhal Magsi</option>
											<option value="Kacchi">Kacchi</option>
											<option value="Kalat">Kalat</option>
											<option value="Kech">Kech</option>
											<option value="Kharan">Kharan</option>
											<option value="Khuzdar">Khuzdar</option>
											<option value="Killa Abdullah">Killa Abdullah</option>
											<option value="Killa Saifullah">Killa Saifullah</option>
											<option value="Kohlu">Kohlu</option>
											<option value="Lasbela">Lasbela</option>
											<option value="Lehri">Lehri</option>
											<option value="Loralai">Loralai</option>
											<option value="Mastung">Mastung</option>
											<option value="Musakhel">Musakhel</option>
											<option value="Nasirabad">Nasirabad</option>
											<option value="Nushki">Nushki</option>
											<option value="Panjgur">Panjgur</option>
											<option value="Pishin Valley">Pishin Valley</option>
											<option value="Quetta">Quetta</option>
											<option value="Sherani">Sherani</option>
											<option value="Sibi">Sibi</option>
											<option value="Sohbatpur">Sohbatpur</option>
											<option value="Washuk">Washuk</option>
											<option value="Zhob">Zhob</option>
											<option value="Ziarat">Ziarat</option>
                                           
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="col-md-12 mb-0">
                                    <div class="form-group">
                                        <label for="Phone">Phone <span class="text-danger">*</span></label>
                                        <input type="text" name="phone" id="lastname" class="form-control" >
                                    </div>
                                </div>

                                <div class="col-md-12 mb-0">
                                    <div class="form-group">
                                        <label for="Email_address">Email address <span class="text-danger">*</span></label>
                                        <input type="text" name="email" id="lastname" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-0">
                                    <strong class="txt-orange">Additional information</strong>
                                </div>
                                
                                <div class="col-md-12 mb-0">
                                    <div class="">
                                        <label for="notes">Order notes (optional)</label>
                                        <textarea name="notes" id="comment" class="form-control" rows="6" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="place-order-wrap">
                            <h2 class="fw-7 txt-blue">Your Order</h2>

                            <div class="order-head">
                                <span>Product</span>
                                <span>Subtotal</span>
                            </div>

                            <div class="order-list">
                                <ul class="list-unstyled">
                                	@foreach($cartItems as $row)
                                    <li>
                                        <span>{{@$row->product_name}}</span>
                                        <span class="txt-green"><?php echo $row->price * $row->product_quantity;  ?> </span>
                                    </li>
                                    @endforeach
                                    <li>
                                        <span>Subtotal</span>
                                        <span class="txt-green"> {{@$total}} Rs</span>
                                    </li>
                                    <li>
                                        <span>Total</span>
                                        <span class="txt-green">{{@$total}} Rs</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="order-disclaimer">
                                <div class="info">
								
 									<select name="payment_method" id="payment_method" class="form-control" required="">
 									<option value="Cash On Delivery">Cash On Delivery</option>
 									<option  value="Jazz Cash">Jazz Cash</option>
									<option value="Easy Pasia">Easy Pasia</option>
									<option value="Bank Transfer">Bank Transfer</option>
									</select>

									<br>
                                </div>
                                <div class="info" >
                                	<div id="banktransfer">
                                	<strong><p>Account Title : {{@$bank->accounttitle}} </p>
                                	<p>Account Number : {{@$bank->accountnumber}} </p></strong>
                                     </div>  
                                     <div id="jazzcash"> 
                                     <strong><p>Account Title : {{@$jazzcash->accounttitle}} </p>
                                	<p>Account Number : {{@$jazzcash->accountnumber}} </p></strong>
                                     </div>

                                     <div class="info" id="easypaisa">
                                	<strong><p>Account Title : {{@$easypaisa->accounttitle}} </p>
                                	<p>Account Number : {{@$easypaisa->accountnumber}} </p></strong>
                                     </div>
                                 
                                    <div id="recipetupload" style="display: none;">
                                    <input type="hidden" name="bank_receipt" id="bankimage">
                                	<input type="file" id="bankpayment" class="form-control" >
                                	<br>
                                	<p>Upload recipet. Our team will verify it in next 24 hours</p> 
                                	</div>                               	

                                </div>

                                 
                                

                                <div class="text-right">
                                    <button type="submit" class="btn-theme bg-green mt-3">PLACE ORDER</button>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
               </form>
            </div>
             @else
			<center>
			<h2>
			Your Cart Is Empty.
			</h2> 
		   </center>
		    @endif
        </section>

       
        <!-- Billing Details End -->

    </main>







@include('front.footer')



<script type="text/javascript">
    $( document ).ready(function() {
		$('#banktransfer').hide();
		$('#easypaisa').hide();
		$('#jazzcash').hide();
        
		$('#payment_method').on('change', function() {
		 //alert( 'this.value' ); // or $(this).val()
		if(this.value == "Bank Transfer") {
		$('#banktransfer').show();
         $('#recipetupload').css('display','inline');

		$('#easypaisa').hide();
		$('#jazzcash').hide();
		}
		else if(this.value == "Jazz Cash") {
		$('#jazzcash').show();
         $('#recipetupload').css('display','inline');
		$('#banktransfer').hide();
		$('#easypaisa').hide();



		}
		else if(this.value == "Easy Pasia") {
		$('#easypaisa').show();
         $('#recipetupload').css('display','inline');
		$('#banktransfer').hide();
		$('#jazzcash').hide();


		}
		else {
		$('#banktransfer').hide();
		$('#easypaisa').hide();
		$('#jazzcash').hide();
         $('#recipetupload').css('display','none');
     

		}
		});
    });

   
</script>


<div class="modal fade" id="imageUpload" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
        <div class="modal-content">

        <div class="modal-body">
        <center>  <img src="https://cdn.dribbble.com/users/419257/screenshots/1724076/scanningwoohoo.gif" height="200px"> </center>
        <!--           <progress val="0" id="progress-bar"   style="display:none"></progress>
        -->        
        </div>
        </div>      
    </div>
  </div>  

    
 


 
<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-analytics.js"></script>
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-storage.js"></script>
    <script type="text/javascript">
const firebaseConfig = {
  apiKey: "AIzaSyCJixfHrcMJKK0ewT-FIbONJO1hwepQvn4",
  authDomain: "petpat-e9c67.firebaseapp.com",
  projectId: "petpat-e9c67",
  storageBucket: "petpat-e9c67.appspot.com",
  messagingSenderId: "576389095371",
  appId: "1:576389095371:web:b08c97c1292d780d72a04e",
  measurementId: "G-J4QB0LREH0"
};
 $(document).ready(function () {
   firebase.initializeApp(firebaseConfig);
   $("#bankpayment").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#bankimage").attr("data-imageUrl", downloadURL);
            $("#bankimage").val(downloadURL)
          })
      }
   })

})
const uploadImageOnFirebase = function (file,) {
   return new Promise((resolve, reject) => {
      const fileExtension = file.name.split('.').slice(-1).pop();
      let filename = $.now() + Math.floor(Math.random() * 10000) + '.' + fileExtension;
      var storageRef = firebase.storage().ref('images/'+filename);
      var uploadTask = storageRef.put(file);
      uploadTask.on('state_changed',
         function progress(snapshot){
                // $(':input[type="button"]').prop('disabled', true);
               // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log(progress);
             // $('#uploader').val(progress);
            switch (snapshot.state) {
               case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
               break;
               case firebase.storage.TaskState.RUNNING: // or 'running'
                  $('#imageUpload').modal('show');
                  /*$('#progress-bar').val(progress);
                  $('#progress-bar').css('display','inline');*/
                  $('#imageUpload').modal('hide');


               break;
            }
            },
            function error(err){
            reject(err);
            },
         function complete() {
            uploadTask.snapshot.ref.getDownloadURL()
               .then(function (downloadURL) {
                $('#progress-bar').css('display','none');
                  resolve(downloadURL);
               })
            }

      )
   })
}
 </script>