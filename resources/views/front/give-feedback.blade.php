@include('front.header')

<section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page">
            <img class="ptt-png" src="{{url('images/Dot-Shape.png')}}" alt="png">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Feedback</li>
                    </ol>
                </nav>
                <h1>Feedback</h1>
            </div>
        </div>
    </section>



     <section class="wide-tb-150 pb-0 mb-5">
     	
            <div class="container">
				@if(session('message'))
				<p class="alert alert-warning">
				{{session('message')}}</p>
				@endif
                <h1 class="heading-main center wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">

                     <span>Share your experience</span> with us
                </h1>
                <div class="row">
                    
                    <div class="col-lg-10 col-md-12 mx-auto">
                        <div class="need-help mt-0">
                            <form action="{{url('/post-feedback')}}" method="post" >
                            	@csrf
                                <div class="row">
                                    <div class="col-md-6 mb-0">
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Full name" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mb-0">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Email address" >
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-0">
                                        <div class="form-group">
                                            <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone number">
                                        </div>
                                    </div>
 

                                    <div class="col-md-12 mb-0">
                                        <div class="form-group">
                                            <textarea name="feedback" id="comment" class="form-control" rows="6" placeholder="Write your message..." required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn-theme bg-orange capusle">Submit Feedback</button>
                                    </div>
                                </div>
                                
                            </form>

                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <br>







@include('front.footer')
