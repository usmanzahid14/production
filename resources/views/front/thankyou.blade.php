@include('front.header')

<div class="jumbotron text-center" style="margin-top: 20%;">
  <h1 class="display-3">Thank You!</h1>
  <p class="lead"><strong>Please check your account</strong> for your order status.</p>
  <hr>
   
  <p class="lead">
    <a href="{{url('/')}}" class="btn-theme bg-navy-blue mt-4">Shop More</a>  
  </p>
</div>




@include('front.footer')
