<?php

$scratchpost = DB::table('category')->where('parent','Scratch Post')->get();
$house = DB::table('category')->where('parent','Houses')->get();
$cat_food = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Food')->get();
$cat_litter = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Litter & Wastage')->get();
$cat_bath = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Bath Shower')->get();
$cat_acc = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Accessories')->get();
$cat_groom = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Grooming Supplies')->get();
$cat_vet = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Veterinary')->get();

$dog_food = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Food')->get();
$dog_bath = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Bath Shower')->get();
$dog_acc = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Accessories')->get();
$dog_groom = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Grooming Supplies')->get();
$dog_vet = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Veterinary')->get();

use App\Http\Controllers\IndexController;
$cart = IndexController::cartCount();
$cartItems = IndexController::cartItems();
$totalAmount = IndexController::cartAmount();

$social = DB::table('social_links')->where('id',1)->first();

 ?>

<!doctype html>
<html lang="en">

<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- xxx Change With Your Information xxx -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no" />
    <title>Petpat.pk</title>
    <meta name="author" content="">
    <meta name="description"
        content="PetPat.pk is Animals Shop & Pets related services.">
    <meta name="keywords"
        content="PetPat.pk,cats scratchpost,cat scratchers, animal care, cats, Dog grooming, dogs, pet, pet care, pet center, pet services, pet shelter, pet shop, pets accessories, cat food, pets store, dog food, veterinary,food,birds">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{url('images/favicon.ico')}}">


    <!-- Main Style CSSS -->
    <link href="{{url('css/theme-plugins.min.css')}}" rel="stylesheet">
    <!-- Main Theme CSS -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <!-- Responsive Theme CSS -->
    <link href="{{url('css/responsive.css')}}" rel="stylesheet">

    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('revolution/css/navigation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('revolution/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
		<link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome/css/font-awesome.css')}}">
		

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
 
    <!-- Page loader Start -->
       <div id="pageloader">
        <div class="loader-item">
            <div class="loader-item-content">
               <img src="{{url('images/logo_dark.png')}}" alt=""   >
            </div>
        </div>
    </div>    
    <!-- Page loader End -->

    <b class="screen-overlay"></b>

    <header class="home-shop">
        <div class="shop-top-bar">
            <i data-feather="tag"></i> Get all the new stuff at discounted price. Shop with confidence today!
        </div>
        <div class="container">
            <div class="home-shop-header">
                <div>
                    <a class="navbar-brand rounded-bottom light-bg" href="{{url('/')}}">
                        <img src="{{url('images/logo_dark.png')}}" alt=""  style="height: 100px  !important;">
                    </a>
                </div>
                <div class="header-option">
                    <!-- <a class="nav-link ml-auto" href="#" id="search_home"><i data-feather="search"></i></a> -->
                       <!--  <form id="custom-search-form" class="form-search form-horizontal pull-right">
                        <div class="input-append span12">
                        <input type="text" class="search-query mac-style" placeholder="Search">
                        <button type="submit" class="btn"><img src="" style="height: 35px;"></button>
                        </div>
                        </form> -->
                    <a class="nav-link" href="#" id="shopping-bag" data-trigger="#card_mobile"><img src="{{url('images/images.png')}}" style="height: 30px !important;"> <span class="badge badge-success">{{@$cart}}</span></a>
                     <?php
                    $session = Session::get('user_id');
                    if($session)
                    {?>
                      <a href="{{url('customers/dashboard')}}" class="btn-theme bg-green btn-shadow ml-4 d-none d-sm-block text-capitalize">My Account</a>
                    <?php }
                    else
                    {?>
                      <a href="{{url('login-register')}}" class="btn-theme bg-green btn-shadow ml-4 d-none d-sm-block text-capitalize">Login / Register</a>                     
                    <?php }
                     ?>
                   


                    <!-- Toggle Button Start -->
                    <button class="navbar-toggler x collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>            
        </div>
        <!-- Main Navigation Start -->
        <nav class="navbar navbar-expand-lg nav-light nav-oval header-fullpage">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarCollapse" data-hover="dropdown"
                    data-animations="slideInUp slideInUp slideInUp slideInUp">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle-mob" href="{{url('/')}}" 
                                >Home </a>
                            
                        </li>
                         <li class="nav-item dropdown megamenu">
                            <a href="#" class="nav-link dropdown-toggle-mob" id="dropdown07"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Scratch Post <i class="icofont-thin-down"></i></a>
                            <div class="menu-back-div dropdown-menu megamenu-content" role="menu" aria-labelledby="dropdown07">
                                <div class="megamenu-content-wrap">
                                    <ul class="list-unstyled">
                                        @foreach($scratchpost as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{$row->name}} </a></li>
                                        @endforeach
                                      
                                    </ul>
                                       
                                
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown megamenu">
                            <a href="#" class="nav-link dropdown-toggle-mob" id="dropdown06"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Houses <i class="icofont-thin-down"></i></a>
                            <div class="menu-back-div dropdown-menu megamenu-content" role="menu" aria-labelledby="dropdown06">
                                <div class="megamenu-content-wrap">
                                    <ul class="list-unstyled">
                                        @foreach($house as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{$row->name}}</a></li>
                                        @endforeach                                 
                                       
                                
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown megamenu">
                            <a href="#" class="nav-link dropdown-toggle-mob" id="dropdown04"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cat Products <i class="icofont-thin-down"></i></a>
                            <div class="menu-back-div dropdown-menu megamenu-content" role="menu" aria-labelledby="dropdown04">
                                <div class="megamenu-content-wrap">
                                    <ul class="list-unstyled">
                                        <h4>Food</h4>
                                        @foreach($cat_food as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}"> {{@$row->name}}</a></li>
                                        @endforeach
                                    

                                    </ul>
                                    <ul class="list-unstyled">
                                       <h4>Litter & Wastage</h4>
                                       @foreach($cat_litter as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{$row->name}}</a></li>
                                        @endforeach        

                                         <h4>Bath Shower</h4>
                                        @foreach($cat_bath as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{$row->name}}</a></li>
                                        @endforeach     
                                         

                                    </ul>
                                     <ul class="list-unstyled">
                                        <h4>Accessories</h4>
                                        @foreach($cat_acc as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{@$row->name}}</a></li>
                                        @endforeach
                                       
                                        
                                    </ul>
                                    <ul class="list-unstyled">
                                        <h4>Grooming Supplies</h4>
                                         @foreach($cat_groom as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{@$row->name}}</a></li>
                                        @endforeach
                                       
                                        

                                         <h4>Veterinary</h4>
                                        @foreach($cat_vet as $row)
                                        <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}">{{@$row->name}}</a></li>
                                        @endforeach
                                          
                                        
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown megamenu">
                            <a href="#" class="nav-link dropdown-toggle-mob" id="dropdown05"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dog Products <i class="icofont-thin-down"></i></a>
                            <div class="menu-back-div dropdown-menu megamenu-content" role="menu" aria-labelledby="dropdown05">
                                <div class="megamenu-content-wrap">
                                    <ul class="list-unstyled">
                                        <h4>Food</h4>
                                        @foreach(@$dog_food as $row)
                                         <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}"> {{@$row->name}}</a></li>

                                        @endforeach

                                      

                                         <h4>Bath Shower</h4>
                                          @foreach(@$dog_bath as $row)
                                         <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}"> {{@$row->name}}</a></li>
                                         @endforeach
 

                                         <h4>Veterinary</h4>
                                          @foreach(@$dog_vet as $row)
                                         <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}"> {{@$row->name}}</a></li>
                                          @endforeach
                                       

                                    </ul>
                                   
                                     <ul class="list-unstyled">
                                        <h4>Accessories</h4>
                                         @foreach(@$dog_acc as $row)
                                         <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}"> {{@$row->name}}</a></li>
                                        @endforeach
                                      

                                          <h4>Grooming Supplies</h4>
                                           @foreach(@$dog_groom as $row)
                                         <li><a class="dropdown-item" href="{{url('/products/'.$row->id.'/'.$row->slug)}}"> {{@$row->name}}</a></li>
                                        @endforeach
                                       

                                    
                                        
                                    </ul>
                                   
                                </div>
                            </div>
                        </li>
                         
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle-mob" href="{{url('/blogs')}}" id="blog-menu" >Blogs</a>

                            
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle-mob" href="{{url('/aboutus')}}">About Us</a>

                            
                        </li>
                         
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/contactus')}}">Contact</a>
                        </li>

                    </ul>
                    <div class="navbar-nav ml-auto sitenav-right">
                        <a class="nav-link"  href="{{$social->facebook}}"><i data-feather="facebook"></i></a>                        
                        <a class="nav-link" href="{{$social->twitter}}"><i data-feather="twitter"></i></a>                        
                        <a class="nav-link" href="{{$social->instagram}}"><i data-feather="instagram"></i></a>
                        <a class="nav-link" href="{{$social->youtube}}"><i data-feather="youtube"></i></a>
                    </div>
                    <!-- Main Navigation End -->
                </div>
            </div>
        </nav>        
        <!-- Main Navigation End -->
    </header>

    <article class="card mobile-offcanvas offcanvas-right" id="card_mobile">
        <div class="shop-sidebar">
            <div class="offcanvas-header">  
                <button class="btn btn-close"> <i data-feather="x-circle"></i> </button>
            </div>
            <h3 class="head">My Cart</h3>

             @if(count($cartItems) > 0)

            <ul class="list-unstyled">
                @foreach($cartItems as $row)
                 <?php $cartImage = $row['product']->getMedia('product_image'); ?>
                <li>
                    <img src="{{ ($cartImage[0]->getUrl()) }}" alt="">
                    <div>
                        <h4><a href="{{url('details/'.$row['product']->id)}}">{{@$row['product']->product_name}}</a></h4>

                        <h6><?php echo $row['product']->price * $row['quantity']; ?> Rs</h6>
                    </div>
                    <div class="delete-btn">
                        <a href="{{url('/delete-cart-item/'.$row['cart_id'])}}"><i class="icofont-close-line"></i></a>
                    </div>
                </li>
                @endforeach
                 
            </ul>
            <div class="sidebar-subtotal">
                <span>Subtotal</span> 
                <strong>{{$totalAmount}} Rs </strong>
            </div>
            <div class="btn-holder">
                <div class="col">
                    <a href="{{url('shoping-cart')}}" class="btn-theme bordered bg-navy-blue btn-sm btn-block">View Cart</a>
                </div>
                <div class="col">
                    <a href="{{url('/checkout')}}" class="btn-theme bg-orange btn-shadow btn-sm btn-block">Checkout</a>
                </div>
            </div>

             @else
            <center>
            <h4>
            Your Cart Is Empty.
            </h4> 
           </center>
            @endif



        </div>
    </article>



    