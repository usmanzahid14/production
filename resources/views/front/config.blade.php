<?php

$scratchpost = DB::table('category')->where('parent','Scratch Post')->get();
$house = DB::table('category')->where('parent','Houses')->get();
$cat_food = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Food')->get();
$cat_litter = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Litter & Wastage')->get();
$cat_bath = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Bath Shower')->get();
$cat_acc = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Accessories')->get();
$cat_groom = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Grooming Supplies')->get();
$cat_vet = DB::table('category')->where('parent','Cat Products')
->where('subcategory','Veterinary')->get();

$dog_food = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Food')->get();
$dog_bath = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Bath Shower')->get();
$dog_acc = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Accessories')->get();
$dog_groom = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Grooming Supplies')->get();
$dog_vet = DB::table('category')->where('parent','Dog Products')
->where('subcategory','Veterinary')->get();

use App\Http\Controllers\IndexController;
$cart = IndexController::cartCount();
$cartItems = IndexController::cartItems();
$totalAmount = IndexController::cartAmount();

$social = DB::table('social_links')->where('id',1)->first();

 ?>