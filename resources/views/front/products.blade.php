@include('front.header')
  
    <!-- Page Breadcrumbs Start -->
    <section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page">
            <img class="ptt-png" src="{{url('images/images/Dot-Shape.png')}}">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$category->parent}}</li>
                    </ol>
                </nav>
                <h1>{{@$category->name}}</h1>
            </div>
        </div>
    </section>
    <!-- Page Breadcrumbs End -->

    <!-- Main Body Content Start -->
    <main id="body-content">

        <!-- Shopping Wide Start -->
        <section class="wide-tb-100 pb-0">
            <div class="container">
               <!--  <div class="product-count">
                    <div class="row align-items-center">
                        <div class="col-xl-9 col-lg-8 col-md-6 mb-2 mb-md-0">
                            <strong>Showing 1–15 of 23 results</strong>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <select name="orderby" class="form-control wide form-light">
                                <option selected="" value="1">Default sorting</option>
                                <option value="2">Sort by popularity</option>
                                <option value="3">Sort by newness</option>
                                <option value="4">Sort by price: low to high</option>
                                <option value="5">Sort by price: high to low</option>
                                <option value="6">Product Name: Z</option>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 row-cols-sm-1">
                    @foreach($products as $row)
                    <div class="col mb-5" style="height: 410px !important;">
                        <div class="product-grid">
                            <div class="product-image">
                                <a href="{{url('details/'.$row->id)}}" class="image">
                                    <?php $image = $row->getMedia('product_image'); ?>
                                    <img class="pic-1" src="{{$image[0]->getUrl()}}" style="height: 300px !important;">
                                </a>
<!--                                 <span class="product-discount-label">-33%</span>
 -->                               <!--  <ul class="product-links">
                                    <li><a href="#" data-tip="Add to Wishlist"><i data-feather="eye"></i></a></li>
                                    <li><a href="#" data-tip="Quick View"><i data-feather="heart"></i></a></li>
                                </ul> -->
                                <a class="add-to-cart btn-theme bg-navy-blue capusle addToCart" href="{{url('/addtocart/'.$row->id)}}" >add to cart <span class="ml-2"> <i data-feather="shopping-bag"></i></span></a>
                            </div>
                            <div class="product-content">
                                <h3 class="title"><a href="{{url('details/'.$row->id)}}">{{@$row->product_name}}</a></h3>
                                <div class="price"><span></span>{{@$row->price}}</div>                                
                            </div>
                        </div>
                    </div>
                    @endforeach
               
                </div>
                <div class="my-3">
                    <!-- <div class="theme-pagination">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><i data-feather="arrow-left"></i></span>
                                    </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true"><i data-feather="arrow-right"></i></span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- Shopping Wide End -->

    </main>
@include('front.footer')
