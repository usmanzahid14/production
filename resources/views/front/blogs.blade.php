 
@include('front.header')




 <main id="body-content" style="margin-top: 20%">

        <!-- Shopping Wide Start -->
        <section class="wide-tb-100 pb-0">
            <div class="container">
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-sm-2">
                    @foreach($blogs as $blog)
                    <div class="col mb-5">
                        <div class="blog-wrap-modern">
                            <div class="img">
                                <a href="{{url('blog-detail/'.$blog->id)}}"><img src="{{@$blog->blogimage}}" alt="" style="height: 215px;"></a>
                            </div>
                            <div class="content">
                                
                                <h3 class="title">
                                    <a href="{{url('blog-detail/'.$blog->id)}}">{{@$blog->title}}</a>
                                </h3>
                               <!--  <div class="description">
                                    <p>{{@$blog->description}}</p>
                                </div>  -->   
                                <div class="bottom-content">
                                    <div class="thumb-author">
                                        <img src="{{@$blog->authorimage}}" alt="" >
                                        {{@$blog->authorname}}
                                    </div>
                                    <div class="date">{{@$blog->created_at}}</div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    @endforeach

                   
                </div>
                 
                
            </div>
        </section>
        <!-- Shopping Wide End -->

    </main>

@include('front.footer')
