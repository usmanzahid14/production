@include('front.header')


 
    <!-- Page Breadcrumbs Start -->
    <section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page">
            <img class="ptt-png" src="{{url('/images/Dot-Shape.png')}}" alt="png">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$blog->title}}</li>
                    </ol>
                </nav>
                <h1>{{@$blog->title}}</h1>
            </div>
        </div>
    </section>
    <!-- Page Breadcrumbs End -->

    <!-- Main Body Content Start -->
    <main id="body-content">

        <!-- Shopping Wide Start -->
        <section class="wide-tb-100 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-wrap-modern single-entry">
                            <div class="img">
                                <img src="{{@$blog->blogimage}}" alt="">
                            </div>
                            <div class="content">
                                
                                <h3 class="title">
                                    {{@$blog->title}}
                                </h3>
                                <div class="description">
                                </div>    
                                <div class="bottom-content">
                                    <div class="thumb-author">
                                        <img src="{{@$blog->authorimage}}" alt="">
                                       {{@$blog->authorname}}
                                    </div>
                                    <div class="date">{{@$blog->created_at}}   <a href="#">2 Views</a></div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="entry-text-gap">
                            <p><?php
                              echo html_entity_decode($blog->description);

                             ?></p>
                        </div>

                       

                        <!-- Tags & Share Box -->
                        <div class="row align-items-center">
                            <div class="col-md-auto">
                                <div class="tags">
                                    <a href="#">{{@$blog->tag}}</a>
                                </div>
                            </div>
                          <!--   <div class="col-md-auto ml-auto">
                                <div class="share-this">
                                    <div class="d-inline-flex align-items-center">
                                        Share it:
                                        <a href="#" class="rounded-circle tw"><i class="icofont-twitter"></i></a>
                                        <a href="#" class="rounded-circle ff"><i class="icofont-facebook"></i></a>
                                        <a href="#" class="rounded-circle ln"><i class="icofont-linkedin"></i></a>
                                    </div>
                                </div>
                            </div> -->
                        </div>

                       <!--   <div class="row">
                            <div class="col-sm-6 text-center">
                                <img src="{{url('/images/blog/blog_single_1.jpg')}}" class="rounded" alt="">
                            </div>
                            <div class="col-sm-6 text-center">
                                <img src="{{url('/images/blog/blog_single_2.jpg')}}" class="rounded" alt="">
                            </div>
                        </div> -->
                        <!-- Tags & Share Box -->

                        <!-- <div class="author-box">
                            <div class="media">
                                <div class="thumb">
                                    <img src="{{url('/images/testimonial_thumb_large_1.jpg')}}" alt="" class="rounded-circle">
                                </div>
                                <div class="service-inner-content media-body pos-rel">
                                    <div class="social-icon-author">
                                        <a href="#"><i class="icofont-twitter"></i></a>
                                        <a href="#"><i class="icofont-facebook"></i></a>
                                        <a href="#"><i class="icofont-instagram"></i></a>
                                    </div>
                                    <h5 class="fw-7 txt-white mb-3">About Mila Flowers</h5>
                                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining unchanged. It was popularised in the sheets containing.
                                </div>
                            </div>
                        </div> -->

                        <!-- Comments List -->
                        <!-- <div class="commnets-reply">
                            <h3 class="txt-orange fw-7 mb-3">Comments (02)</h3>
                            <div class="media">
                                <img class="thumb" src="{{url('/images/blog/comment_thumg_1.jpg')}}" alt="">
                                <div class="media-body">
                                    <div class="name">
                                        <small>January 17, 2020</small>
                                        <h5>Cindy Cambell</h5>
                                        <a href="#" class="btn-theme bg-green btn-sm text-capitalize">Reply</a>
                                    </div>
                                    <p>3 years ago we wanted to go to see Michalengelos David in the Academy in Florence. The line was down the block</p>
                                    <p></p>
                                </div>
                            </div>

                            <div class="media reply">
                                <img class="thumb" src="{{url('/images/blog/comment_thumg_2.jpg')}}" alt="">
                                <div class="media-body">
                                    <div class="name">
                                        <small>January 17, 2020</small>
                                        <h5>Harry Olson</h5>
                                        <a href="#" class="btn-theme bg-green btn-sm text-capitalize">Reply</a>
                                    </div>
                                    <p>3 years ago we wanted to go to see Michalengelos David in the Academy in Florence. The line was down the block</p>
                                    <p></p>
                                </div>
                            </div>

                            <div class="media">
                                <img class="thumb" src="{{url('/images/blog/comment_thumg_1.jpg')}}" alt="">
                                <div class="media-body">
                                    <div class="name">
                                        <small>January 17, 2020</small>
                                        <h5>Cindy Cambell</h5>
                                        <a href="#" class="btn-theme bg-green btn-sm text-capitalize">Reply</a>
                                    </div>
                                    <p>3 years ago we wanted to go to see Michalengelos David in the Academy in Florence. The line was down the block</p>
                                    <p></p>
                                </div>
                            </div>
                            
                        </div> -->
                        <!-- Comments List -->
                        
                        <!-- Reply Comment Form -->
                       <!--  <div class="comment-reply-form">
                            <h3 class="txt-white fw-7 mb-3">Leave a Comment</h3>
                            <form action="#" method="post" novalidate="novalidate" class="rounded-field">
                                <div class="form-row mb-4">
                                    <div class="col">
                                        <textarea rows="7" placeholder="Message" class="form-control form-light"></textarea>
                                    </div>
                                </div>
                                <div class="form-row mb-4">
                                    <div class="col">
                                        <input type="text" name="name" class="form-control form-light" placeholder="Your Name">
                                    </div>
                                    <div class="col">
                                        <input type="text" name="email" class="form-control form-light" placeholder="Email">
                                    </div>
                                </div>                                
                                <div class="form-row">
                                    <button type="submit" class="btn-theme bg-navy-blue capusle mb-3">Post Comment</button>
                                </div>
                            </form>
                        </div> -->
                        <!-- Reply Comment Form -->
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <aside class="row sidebar-widgets">
                            <!-- Sidebar Primary Start -->
                            <div class="sidebar-primary col-lg-12 col-md-6">
                                
                                
                                <!-- Widget Wrap -->
                                <div class="widget-wrap">
                                    <h3 class="widget-title">Popular Posts</h3>
                                    
                                    <div class="popular-post post-thumb">
                                        <ul class="list-unstyled">
                                            @foreach($blogs as $row)
                                            <li>
                                                <img src="{{@$row->blogimage}}" alt="{{@$row->title}}">
                                                <div>
                                                    <a href="{{url('blog-detail/'.$row->id)}}" class="title">{{@$row->title}}</a>
                                                    <small>{{@$row->created_at}}</small>
                                                </div>
                                            </li>
                                            @endforeach
                                            
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Widget Wrap -->
                            </div>
                            <!-- Sidebar Primary End -->

                            <!-- Sidebar Secondary Start -->
                             
                            <!-- Sidebar Secondary End -->

                            
                        </aside>
                    </div>
                </div>
                
            </div>
        </section>
        <!-- Shopping Wide End -->

    </main>


@include('front.footer')
