 <?php  
$social = DB::table('social_links')->where('id',1)->first();
$imagesetting = DB::table('image_settings')->where('id',1)->first();



 ?>

  <!-- Main Footer Start -->
    <div class="container footer-top-callout">
        <div class="row">
            <!-- Callout Section Side Image -->
            <div class="col-sm-12">
                <div class="callout-style-side-img d-lg-flex align-items-center">
                    <div class="img-callout">
                        <img src="{{@$imagesetting->image10}}" alt="Pets Store">
                    </div>
                    <div class="text-callout">
                        <div class="d-md-flex align-items-center">
                            <div class="icon">
                                <i data-feather="headphones"></i>
                            </div>
                            <div class="heading">
                                <h3>Have Questions? Call Us <span class="txt-green">+92 33123456789</span>
                                </h3>
                                <div>If you have any queries or innovative ideas/suggestions to help us improve drop them by</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Callout Section Side Image -->
        </div>
    </div>
    <footer class="wide-tb-150 bg-navy-blue pb-0">
        <div class="container">
            <div class="row wide-tb-50 pt-0 align-items-center">
                <div class="col-md-6 mb-0">
                    <div class="logo-footer">
                        <img src="{{url('images/logo_dark.png')}}" alt="" style="height: 150px;">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="social-icons d-flex justify-content-md-end align-items-center">
                        We are social
                        <ul class="list-unstyled list-group list-group-horizontal">
                            <li><a href="{{$social->twitter}}"><i data-feather="twitter"></i></a></li>
                            <li><a href="{{$social->facebook}}"><i data-feather="facebook"></i></a></li>
                            <li><a href="{{$social->instagram}}"><i data-feather="instagram"></i></a></li>
                            <li><a href="{{$social->youtube}}"><i data-feather="youtube"></i></a></li>
 

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Column First -->
                <div class="col-lg-4 col-md-6 mr-auto">
                    <h3 class="footer-heading">About Petpat.pk</h3>
                    <p>Petpat.pk is all in one store of pet supplies, toys, and other related products. You can find any sort of pet product at our online store. We offer a large inventory of exclusive pet products and supplies of different international brands. All the products will be delivered to your doorstep.</p>
                </div>
                <!-- Column First -->

                <!-- Column Second -->
                <div class="col-lg-2 col-md-6">
                    <h3 class="footer-heading">Explore</h3>
                    <div class="footer-widget-menu">
                        <ul class="list-unstyled">
                            <li><a href="{{url('/aboutus')}}"><i class="icofont-simple-right"></i> <span>About Us</span></a></li>
                            <li><a href="{{url('/')}}"><i class="icofont-simple-right"></i> <span>Shop</span></a></li>
                            <li><a href="{{url('/contactus')}}"><i class="icofont-simple-right"></i> <span>Contact us</span></a></li>
                          
                        </ul>
                    </div>
                </div>
                <!-- Column Second -->

               

                <!-- Column Fourth -->
                <div class="col-lg-3 ml-auto col-md-6">
                    <h3 class="footer-heading">Working Hours</h3>
                    <p>Our support available to help you 24 hours a day. We provide our best.
                    </p>
                    <p>
                        If you have any queries you can contact us.

                    </p>

                     
                </div>
                <!-- Column Fourth -->
            </div>
        </div>

        <div class="copyright-wrap">
            <div class="container pos-rel">
                <div class="copyright-text">
                    <div>Copyrights 2021 <span class="txt-green">PetPat.pk</span> All Rights Reserved.</div>
                    
                </div>
                <div class="footer-dog">
                    <img src="{{@$imagesetting->image8}}" alt="pets shop" style=" height:150px;">
                </div>
            </div>
        </div>
    </footer>
    <!-- Main Footer End -->

    <!-- Back To Top Start -->
    <a id="mkdf-back-to-top" href="#" class="off"><i class="icofont-rounded-up"></i></a>
    <!-- Back To Top End -->


        <!-- Jquery Library JS -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/theme-plugins.min.js')}}"></script>    
    <!-- Theme Custom FIle -->
    <script src="{{url('js/site-custom.js')}}"></script>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="{{url('revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/jquery.themepunch.revolution.min.js')}}"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{url('revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

    <script type="text/javascript">
        
        var tpj=jQuery;
        var revapi26;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_26_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_26_1");
            }else{
                revapi26 = tpj("#rev_slider_26_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"revolution/js/",
                    sliderLayout:"auto",
                    dottedOverlay:"black",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                         mouseScrollReverse:"default",
                        onHoverStop:"off",
                        touch:{
                            touchenabled:"on",
                            touchOnDesktop:"off",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"metis",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:778,
                            hide_onleave:false,
                            tmp:'',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:15,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:15,
                                v_offset:0
                            }
                        }
                        ,
                        bullets: {
                            enable:true,
                            hide_onmobile:false,
                            style:"zeus",
                            hide_onleave:false,
                            direction:"horizontal",
                            h_align:"center",
                            v_align:"bottom",
                            h_offset:0,
                            v_offset:30,
                            space:5,
                            tmp:''
                        }
                    },
                    responsiveLevels:[1400,1200,991,480],
                    visibilityLevels:[1400,1200,991,480],
                    gridwidth:[1400,1200,991,480],
                    gridheight:[600,700,700,600],
                    lazyType:"none",
                    parallax: {
                        type:"scroll",
                        origo:"slidercenter",
                        speed:2000,
                        levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
                    },
                    shadow:0,
                    spinner:"spinner0",
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,
                    shuffle:"off",
                    autoHeight:"on",
                    fullScreenAutoWidth:"off",
                    fullScreenAlignForce:"off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "0px",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        }); /*ready*/
    </script>

   <!--  <script type="text/javascript">
    $( document ).ready(function() {
      $(document).on('click', '.addToCart', function(e){
        data =$(this).data('id');
        //alert($(this).data('id'));

        $.ajax({
        method:"GET",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        modal: true,

        url:'addtocart/'+data,
         
        success:function(data){

        if(data == 0)
        {
         window.location.href = "http://127.0.0.1:8000/login-register";

        }
        else
        {

            $("#success").modal('show');
        } 
          
        },
        error: function()
        {
        alert('Something Went Wrong. Please try again later. If the issue persists contact support.');

        }
        });

        });
      

    //alert( "ready!" );
    });
    </script> -->



    <!-- Modal -->
<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
      <div class="modal-body">
       <center>
        <img src="https://i.pinimg.com/originals/66/22/ab/6622ab37c6db6ac166dfec760a2f2939.gif">
           <h2>
            Successfully Added!
           </h2>
       </center>

      </div>
       
    </div>

  </div>
</div>


 <script type="text/javascript">
    $( document ).ready(function() {
      //alert( "ready!" );
 
        $.ajax({
        method:"GET",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        modal: true,

        url:'{{url('cartajax')}}',
         
        success:function(data){
              
          console.log(data);         
          
        },
        error: function()
        {
        alert('Something Went Wrong. Please try again later. If the issue persists contact support.');

        }
        });

        
    });
    </script>