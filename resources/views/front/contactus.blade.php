@include('front.header')

  <!-- Page Breadcrumbs Start -->
    <section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page" >
            <img class="ptt-png" src="{{url('images/Dot-Shape.png')}}" alt="png">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                    </ol>
                </nav>
                <h1>Contact Us</h1>
            </div>
        </div>
    </section>
    <!-- Page Breadcrumbs End -->

    <!-- Main Body Content Start -->
    <main id="body-content">

        

        <!-- Get in touch with us Start -->
        <section class="wide-tb-100 pb-0">
            <div class="container">
                @if(session('message'))
                <p class="alert alert-warning">
                {{session('message')}}</p>
                @endif
                <h1 class="heading-main center">
                    <small>Get in touch with us <i class="pethund_repeat_grid"></i></small>
                    <span>Make</span> Online Enquiry?
                </h1>
                <div class="row">                    
                    <div class="col-lg-8 col-md-12 mx-auto">
                        <div class="need-help">
                            <div id="sucessmessage"></div>
                            <form action="{{url('/send-contactus')}}" method="post"  >
                            	@csrf
                                <div class="row">
                                    <div class="col-md-6 mb-0">
                                        <div class="form-group">
                                            <input type="text" name="firstname" id="name" class="form-control" placeholder="First Name">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mb-0">
                                        <div class="form-group">
                                            <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mb-0">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Your Email">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mb-0">
                                        <div class="form-group">
                                            <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone Number">
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-0">
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" rows="6" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn-theme bg-green mt-3 capusle">Submit Request</button>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Get in touch with us End -->

        <!-- Client Logos Start -->
        <section class="wide-tb-100 bg-light-gray clients-rounded-wrap need-help-topspace">
            <div class="container pos-rel">
                <div class="contact-map-img">
                    <img src="{{url('images/World-Map-PNG-Picture.png')}}" alt="">
                </div>
                <div class="row">
                   

                    <!-- Icon Boxes Four Style -->
                    <div class="col-md-6 col-sm-6">
                        <div class="icon-box-4 h-100">
                            <i data-feather="phone"></i>
                            <h3>Phone Us</h3>
                            <div>+92 33123456789</div>
                        </div>
                    </div>
                    <!-- Icon Boxes Four Style -->

                    <!-- Icon Boxes Four Style -->
                    <div class="col-md-6 col-sm-6 mx-auto">
                        <div class="icon-box-4 h-100">
                            <i data-feather="mail"></i>
                            <h3>Mail Us</h3>
                            <div><a href="petpatpk@gmail.com">petpatpk@gmail.com</a></div>
                         </div>
                    </div>
                    <!-- Icon Boxes Four Style -->
                </div>
            </div>
        </section>
        <!-- Client Logos End -->

    </main>


    

@include('front.footer')
