@include('front.header')

 
    <section class="home-shop-slider" style="margin-top: 20%;">
        <div id="rev_slider_26_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase"
            data-source="gallery" style="background:#aaaaaa;padding:0px;">
            <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
            <div id="rev_slider_26_1" class="rev_slider fullscreenbanner tiny_bullet_slider" style="display:none;"
                data-version="5.4.1">
                <ul>                    
                    <!-- SLIDE  -->
                    <li data-index="rs-73" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/" data-rotate="0"
                        data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3=""
                        data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                        data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{@$imagesetting->image1}}"
                            data-kenburns="on"
                            data-duration="5000"
                            data-ease="Power3.easeInOut"
                            data-scalestart="110"
                            data-scaleend="100"
                            data-rotatestart="0"
                            style='background:linear-gradient(90deg, rgba(134,143,150,1) 0%, rgba(89,97,100,1) 100%)'
                            alt=""
                            data-bgposition="center right"
                            data-bgfit="cover"
                            data-bgrepeat="no-repeat"
                            data-bgparallax="off"
                            class="rev-slidebg"
                            data-no-retina>
                        <!-- LAYERS -->
        
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption btn-theme bg-orange btn-shadow" id="slide-73-layer-4"
                            data-x="['left','left','left','left']" 
                            data-hoffset="['120','120','120','40']"
                            data-y="['middle','middle','middle','middle']" 
                            data-voffset="['160','120','80','60']"
                            data-width="['180','180','180','130']" 
                            data-height="none" 
                            data-whitespace="normal" 
                            data-type="button"
                            data-responsive_offset="on"
                            data-frames='[{"delay":500,"speed":1000,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
                            data-textAlign="['center','center','center','center']" 
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[30,30,30,20]"
                            data-paddingbottom="[0,0,0,0]" 
                            data-paddingleft="[30,30,30,20]"
                            data-fontsize="['18','18','18','14']"
                            style="z-index: 5; white-space: normal; font-size: 18px; line-height: 50px; font-weight: 400; color: rgba(255,255,255,1); font-family:'Karla', sans-serif;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; font-weight: 600; text-transform: uppercase; box-shadow: 0px 4px 13px 0px rgb(152 203 70 / 50%); background: #98cb46;">
                            <!-- Shop Now --> </div>
        
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme" id="slide-73-layer-1"
                            data-x="['left','left','left','left']" 
                            data-hoffset="['120','120','120','40']"
                            data-y="['middle','middle','middle','middle']" 
                            data-voffset="['40','0','-40','-50']"
                            data-width="['440','180','180','100']" 
                            data-fontsize="['40','40','40','30']"
                            data-whitespace="nowrap" 
                            data-type="text" 
                            data-responsive_offset="on"
                            data-frames='[{"delay":400,"speed":750,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 6; color: #27304b; font-size: 40px; font-weight: 700; font-family: 'Changa', sans-serif;">
                            Good Products for your Pet </div>
        
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption   tp-resizeme" id="slide-73-layer-3" data-x="['left','left','left','left']"
                            data-hoffset="['100','100','100','20']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['90','50','10','0']"
                            data-width="['470','480','470','470']"
                            data-fontsize="['20','20','20','16']"
                            data-height="none"
                            data-whitespace="normal"
                            data-type="text"
                            ata-responsive_offset="on"
                            data-frames='[{"delay":300,"speed":750,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[10,10,10,10]"
                            data-paddingright="[20,20,20,20]"
                            data-paddingbottom="[10,10,10,10]"
                            data-paddingleft="[20,20,20,20]"
                            style="z-index: 7; white-space: normal; font-size: 20px; line-height: 20px; font-weight: 400; color: #ffffff; font-family:Karla', sans-serif;">
                            Quirky Pet Products Your Furry Friend Will Love </div>
        
                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="slide-73-layer-2" 
                            data-x="['left','left','left','left']"
                            data-hoffset="['100','100','100','20']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['20','-20','-60','-50']"
                            data-fontsize="['70','70','60','50']"
                            data-lineheight="['70','70','70','50']"
                            data-width="['600','650','620','380']"
                            data-height="['200','200','200','200']"
                            data-whitespace="normal"
                            data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"delay":200,"speed":750,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[20,20,20,20]"
                            data-paddingright="[20,20,20,20]"
                            data-paddingbottom="[30,30,30,30]"
                            data-paddingleft="[20,20,20,20]"
                            style="z-index: 8; font-family: 'Changa', sans-serif; min-width: 650px; max-width: 650px; white-space : normal; font-size: 80px; line-height: 70px; font-weight: 400; color: #ffffff; letter-spacing: -2px; font-weight: 700;">
                            Want To Have! </div>
                    </li>          
                    <!-- SLIDE  -->
                    <li data-index="rs-75" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/" data-rotate="0"
                        data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3=""
                        data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                        data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{@$imagesetting->image2}}"
                            data-kenburns="on"
                            data-duration="5000"
                            data-ease="Power3.easeInOut"
                            data-scalestart="110"
                            data-scaleend="100"
                            data-rotatestart="0"
                            data-bgcolor='linear-gradient(180deg, rgba(22,160,133,1) 0%, rgba(244,208,63,1) 100%)''
                            style='background:linear-gradient(180deg, rgba(22,160,133,1) 0%, rgba(244,208,63,1) 100%)'
                            alt=""
                            data-bgposition="center center"
                            data-bgfit="cover"
                            data-bgrepeat="no-repeat"
                            data-bgparallax="off"
                            class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
        
                        <!-- LAYER NR. 9 -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption btn-theme bg-orange btn-shadow" id="slide-73-layer-4"
                            data-x="['left','left','left','left']" 
                            data-hoffset="['120','120','120','40']"
                            data-y="['middle','middle','middle','middle']" 
                            data-voffset="['160','120','80','60']"
                            data-width="['180','180','180','130']" 
                            data-height="none" 
                            data-whitespace="normal" 
                            data-type="button"
                            data-responsive_offset="on"
                            data-frames='[{"delay":500,"speed":1000,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
                            data-textAlign="['center','center','center','center']" 
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[30,30,30,20]"
                            data-paddingbottom="[0,0,0,0]" 
                            data-paddingleft="[30,30,30,20]"
                            data-fontsize="['18','18','18','14']"
                            style="z-index: 5; white-space: normal; font-size: 18px; line-height: 50px; font-weight: 400; color: rgba(255,255,255,1); font-family:'Karla', sans-serif;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; font-weight: 600; text-transform: uppercase; box-shadow: 0px 4px 13px 0px rgb(152 203 70 / 50%); background: #98cb46;">
                            <!-- Shop Now --> </div>
        
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme" id="slide-73-layer-1"
                            data-x="['left','left','left','left']" 
                            data-hoffset="['120','120','120','40']"
                            data-y="['middle','middle','middle','middle']" 
                            data-voffset="['40','0','-40','-50']"
                            data-width="['440','180','180','100']" 
                            data-fontsize="['40','40','40','30']"
                            data-whitespace="nowrap" 
                            data-type="text" 
                            data-responsive_offset="on"
                            data-frames='[{"delay":400,"speed":750,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 6; color: #27304b; font-size: 40px; font-weight: 700; font-family: 'Changa', sans-serif;">
                            Everything for every pet </div>
        
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption   tp-resizeme" id="slide-73-layer-3" data-x="['left','left','left','left']"
                            data-hoffset="['100','100','100','20']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['90','50','10','0']"
                            data-width="['470','480','470','470']"
                            data-fontsize="['20','20','20','16']"
                            data-height="none"
                            data-whitespace="normal"
                            data-type="text"
                            ata-responsive_offset="on"
                            data-frames='[{"delay":300,"speed":750,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[10,10,10,10]"
                            data-paddingright="[20,20,20,20]"
                            data-paddingbottom="[10,10,10,10]"
                            data-paddingleft="[20,20,20,20]"
                            style="z-index: 7; white-space: normal; font-size: 20px; line-height: 20px; font-weight: 400; color: #ffffff; font-family:Karla', sans-serif;">
                            Quirky Pet Products Your Furry Friend Will Love </div>
        
                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="slide-73-layer-2" 
                            data-x="['left','left','left','left']"
                            data-hoffset="['100','100','100','20']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['20','-20','-60','-50']"
                            data-fontsize="['70','70','60','50']"
                            data-lineheight="['70','70','70','50']"
                            data-width="['600','650','620','380']"
                            data-height="['200','200','200','200']"
                            data-whitespace="normal"
                            data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"delay":200,"speed":750,"sfxcolor":"#98cb46","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[20,20,20,20]"
                            data-paddingright="[20,20,20,20]"
                            data-paddingbottom="[30,30,30,30]"
                            data-paddingleft="[20,20,20,20]"
                            style="z-index: 8; font-family: 'Changa', sans-serif; min-width: 650px; max-width: 650px; white-space : normal; font-size: 80px; line-height: 70px; font-weight: 400; color: #ffffff; letter-spacing: -2px; font-weight: 700;">
                            For pet's Sake </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom" style="height: 5px; background: rgba(255, 255, 255, 0.15);"></div>
            </div>
        </div>
    </section>

    <!-- Main Body Content Start -->
    <main id="body-content">

        <!-- Welcome To PetHund Start -->
        <section class="wide-tb-150">
            <div class="container">               
                <div class="row align-items-center">                    
                    <div class="col-lg-6">
                        <h1 class="heading-main wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                            <small>Welcome To PetPat.pk <i class="pethund_repeat_grid"></i></small>
                            <span>All Pet Products</span> Under One Roof
                        </h1>

                        <p>Petpat.pk offers a variety of products for a variety of pets. You can find the perfect products for your pet here even if you have a Dog, a Cat, or a feathered pet.</p>

                       <!--  <a href="shop-wide.html" class="btn-theme bg-navy-blue mt-4">Shop Now</a>  -->                       
                    </div>
                    <div class="col-lg-6">
                        <div class="row align-items-center">
                            <div class="col-md-6 wow pulse" data-wow-duration="0" data-wow-delay="0s">
                                <!-- Icon Box Shop -->
                                <div class="icon-box-shop bg-green">
                                    <h3>Accessories</h3>
                                    <p>For accesories for your Best Buddy</p>
                                    <img src="{{@$imagesetting->image3}}" alt="pets accesories" class="ml-auto d-flex">
                                   <!--  <a href="shop-single.html" class="read-more-arrow bg-orange">
                                        Shop Now <span> <i class="icofont-simple-right"></i></span>
                                    </a> -->
                                </div>
                                <!-- Icon Box Shop -->
                            </div>
                            <div class="col-md-6 wow pulse" data-wow-duration="0" data-wow-delay="0s">                                
                                <!-- Icon Box Shop -->
                                <div class="icon-box-shop bg-navy-blue">
                                    <h3>Buy Food</h3>
                                    <p>Healthy food for your pet </p>
                                    <img src="{{@$imagesetting->image4}}" alt="petsfood" class="ml-auto d-flex">
                                   <!--  <a href="shop-single.html" class="read-more-arrow bg-green">
                                        Shop Now <span> <i class="icofont-simple-right"></i></span>
                                    </a> -->
                                </div>
                                <!-- Icon Box Shop -->
                                <!-- Icon Box Shop -->
                                <div class="icon-box-shop bg-orange mt-5 wow pulse" data-wow-duration="0" data-wow-delay="0s">
                                    <h3>NEW COLLECTION</h3>
                                    <p class="m-0">For All Pets With Discount Offer</p>
                                    <img src="{{@$imagesetting->image5}}" alt="pets discounts" class="ml-auto d-flex">
                                    <!-- <a href="shop-single.html" class="read-more-arrow bg-navy-blue">
                                        Shop Now <span> <i class="icofont-simple-right"></i></span>
                                    </a> -->
                                </div>
                                <!-- Icon Box Shop -->
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 wide-tb-100 pb-0">
                        <div class="bg-shadow-white p-4">
                            <div class="row py-4">
                                <div class="col-lg-4 col-md-6">
                                    <div class="icon-box-img d-flex align-items-center">
                                        <div class="icon-img">
                                            <img src="{{url('images/free-shipping.svg')}}" alt="pets">
                                        </div>
                                        <div class="text">
                                            <h3>FREE SHIPPING</h3>
                                            <p>Free shipping on all order</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="icon-box-img d-flex align-items-center">
                                        <div class="icon-img">
                                            <img src="{{url('images/money-back.svg')}}" alt="pets">
                                        </div>
                                        <div class="text">
                                            <h3>MONEY RETURN</h3>
                                            <p>Online support 24 hours</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 mx-auto mt-lg-0 mt-4">
                                    <div class="icon-box-img d-flex align-items-center">
                                        <div class="icon-img">
                                            <img src="{{url('images/support-online.svg')}}" alt="pets">
                                        </div>
                                        <div class="text">
                                            <h3>ONLINE SUPPORT</h3>
                                            <p>Back guarantee under 5 days</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- Welcome To PetHund End -->
        


        <!-- Expertise in following areas Start -->
        <section class="wide-tb-100">
            <div class="container">               
                <h1 class="heading-main center wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                    <small>Expertise in following areas <i class="pethund_repeat_grid"></i></small>
                    <span>Cats</span> Scratchers 
                </h1>

               
                <div class="isotope-gallery captured-img-gallery row">
                    @foreach($catScratchers as $row)
                       <?php $sctrachimage = $row->getMedia('product_image'); ?>

                    <div class="col-lg-3 col-md-4 gallery-item mb-4" style="height: 310px !important;">
                        <div class="product-grid">
                            <div class="product-image">
                                <a href="{{url('details/'.$row->id)}}" class="image">
                                    <img class="pic-1" src="{{ ($sctrachimage[0]->getUrl()) }}" style="height: 200px !important;" alt="catscratchers">
                                </a>
                                 <!-- <span class="product-discount-label">-33%</span> -->  
                              <!--   <ul class="product-links">
                                    <li><a href="#" data-tip="Add to Wishlist"><i data-feather="eye"></i></a></li>
                                    <li><a href="#" data-tip="Quick View"><i data-feather="heart"></i></a></li>
                                </ul> -->
                                <a class="add-to-cart btn-theme bg-navy-blue capusle addToCart" href="{{url('/addtocart/'.$row->id)}}" data-id="{{$row->id}}">add to cart <span class="ml-2"> <i data-feather="shopping-bag"></i></span></a>
                            </div>
                            <div class="product-content">
                                <h3 class="title"><a href="{{url('details/'.$row->id)}}">{{@$row->product_name}}</a></h3>
                                <div class="price"><span>{{@$row->firstprice}} Rs</span> {{@$row->price}}</div>                                
                            </div>
                        </div>
                    </div>
                    @endforeach
                   
                </div>

                <div class="text-center">                    
                    <a href="{{url('products/2/for-female-cat')}}" class="btn-theme bg-navy-blue capusle mt-4">Browse All</a>
                </div>
            </div>
        </section>
        <!-- Expertise in following areas End -->

       


        <section class="wide-tb-100">
            <div class="container">               
                <div class="row align-items-center">
                    <div class="col-lg-5 text-center">
                        <img src="{{@$imagesetting->image6}}" alt="petpat.pk aboutus">
                    </div>
                    <div class="col-lg-7 mt-5 mt-lg-0">
                        <h1 class="heading-main wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                            <small>Quality & Experience <i class="pethund_repeat_grid"></i></small>
                            <span>Petpat.pk</span> Shop
                        </h1>

                        <p>Here at petpat.pk we are proud parents and passionate animal lovers which makes us to better understand our customer’s needs first hand. Our Goal here at petpat.pk is to provide all the pet owners great customer service so that our customers can save their time and be able to buy the desired products for their pets with no hustle.</p>

                        <ul class="list-unstyled icons-listing theme-green paws fw-7 mt-5 txt-blue">
                            <li>Pet supplies</li>
                            <li>Pet Grooming and Cleaning</li>
                            <li>Shop by Brand</li>
                        </ul>

                        <a href="{{url('/aboutus')}}" class="btn-theme bg-navy-blue capusle mt-5 btn-shadow">More About Us</a>                        
                    </div>
                </div>
            </div>
        </section>

         <!-- New Arrivals Start -->
        <section class="wide-tb-100 bg-green">
            <div class="container">
                <h1 class="heading-main center light-mode wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                    <small>Quality & Experience <i class="pethund_repeat_grid"></i></small>
                    New <span class="txt-blue">Arrivals</span>
                </h1>
                <div class="owl-carousel owl-theme" id="home-shop-slider">
                    @foreach($newarrival as $row)
                    <div class="item" style="height: 310px !important;">
                        <div class="product-grid">
                            <div class="product-image">
                                <a href="{{url('details/'.$row->id)}}" class="image">
                                    <?php $image = $row->getMedia('product_image'); ?>
                                    
                                    <img class="pic-1" src="{{ ($image[0]->getUrl()) }}" style=" height: 200px !important;" alt="pets newarrival">
                                 </a>
                                <!-- <span class="product-discount-label">-33%</span> -->
                               <!--  <ul class="product-links">
                                    <li><a href="#" data-tip="Add to Wishlist"><i data-feather="eye"></i></a></li>
                                    <li><a href="#" data-tip="Quick View"><i data-feather="heart"></i></a></li>
                                </ul> -->
                                <a class="add-to-cart btn-theme bg-navy-blue capusle addToCart" href="{{url('/addtocart/'.$row->id)}}" data-id="{{$row->id}}"  >add to cart <span class="ml-2"> <i data-feather="shopping-bag"></i></span></a>
                            </div>
                            <div class="product-content">
                                <h3 class="title"><a href="{{url('details/'.$row->id)}}">{{@$row->product_name}}</a></h3>
                                <div class="price"> <span>{{@$row->firstprice}}</span> {{@$row->price}} Rs</div>                                
                            </div>
                        </div>
                    </div>
                    @endforeach
                 
                </div>
            </div>
        </section>
        <!-- New Arrivals End -->
 

       

        <section class="spacer-70">
            <p>&nbsp;</p>
        </section>


         <!-- Callout Section Parallax -->
        <section class="wide-tb-150 bg-scroll pos-rel " style="background-color: #27304b">
            <div class="bg-overlay black opacity-30"></div>
            <div class="container">
                <div class="text-center">
                    <!-- Heading Main -->
                    <div class="col-sm-12 wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                        <h1 class="heading-main light-mode center">
                            <small>Did you buy anything? <i class="pethund_repeat_grid"></i></small>
                            We Deliver Orders <br> 24/7
                        </h1>
                        <a href="{{url('/give-feedback')}}" class="btn-theme bg-green btn-shadow">Give us feedback</a>
                    </div>
                    <!-- Heading Main -->
                </div>
            </div>
        </section>
        <!-- Callout Section Parallax -->
        <section class="spacer-70">
            <p>&nbsp;</p>
        </section>
        

         <!-- Callout Section Solid Background -->
        <section class="wide-tb-70 bg-navy-blue pt-0 video-popup-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="bg-fixed pos-rel video-popup">
                            <div class="zindex-fixed pos-rel">
                                <a href="{{@$imagesetting->image7}}"><i
                                        class="icofont-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 mx-auto text-center txt-white">
                        <p>Please feel free to take a look at our Website and check what we are offering here. If you have any queries or innovative ideas/suggestions to help us improve drop them by 
                        </p>
                        <div class="text-center mt-4">
                            <a href="contact-us.html" class="btn-theme bg-orange capusle bordered mr-4"><span
                                    class="txt-white">Contact us</span></a>
                            +9233123456789
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Callout Section Solid Background -->
        
        <section class="spacer-70">
            <p>&nbsp;</p>
        </section>
       

        <!-- Amazing Feedback Start -->
        <section class="wide-tb-150 bg-snow pos-rel bg-navy-blue">
            <div class="container">
                <div class="row">
                    <!-- Heading Main -->
                    <div class="col-sm-12 wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                        <h1 class="heading-main light-mode center wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                            <small>What Our Customers Are Saying <i class="pethund_repeat_grid"></i></small>
                            <span>Amazing</span> Feedback
                        </h1>
                    </div>
                    <!-- Heading Main -->
                </div>

                <div class="row align-items-center">                    
                    <div class="col-md-6">
                        <div class="testimonial-rounded">
                            <div class="owl-carousel owl-theme" id="testimonial-rounded">

                                <!-- Client Testimonials Slider Item -->
                                @foreach($feedbacks as $feedback)
                                <div class="item">
                                    <div class="client-testimonial">
                                        <div class="quote-icon">
                                            <i class="pethund_quotes"></i>
                                        </div>
                                        <p>{{$feedback->feedback}} </p>
                                        <div
                                            class="client-testimonial-icon justify-content-center d-flex align-items-center">
                                            
                                            <h3> <span>{{$feedback->name}}</span></h3>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <!-- Client Testimonials Slider Item -->


                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 order-md-first wow fadeInLeft" data-wow-duration="0" data-wow-delay="0s">
                        <img src="{{@$imagesetting->image9}}" alt="PetPat.pk Feedback">
                    </div>
                </div>
            </div>
        </section>
        <!-- Amazing Feedback End -->

         <!-- Recent Blog Post Start -->
        <section class="wide-tb-100">
            <div class="container">
                <h1 class="heading-main center wow fadeInDown" data-wow-duration="0" data-wow-delay="0s">
                    <small>Recent Blog Post <i class="pethund_repeat_grid"></i></small>
                    <span>Latest News </span> & Articles
                </h1>
                
                <div class="owl-carousel owl-theme" id="blog-slider-services">
                    
                    <!-- Blog Slides -->
                    @foreach($blogs as $blog)
                    <div class="item" >
                        <div class="blog-wrap-modern">
                            <div class="img">
                                <a href="{{url('blog-detail/'.$blog->id)}}"><img src="{{@$blog->blogimage}}" alt="{{@$blog->title}}" alt="{{@$blog->title}}" style="height: 290px !important;"></a>
                            </div>
                            <div class="content">                                
                                <h3 class="title">
                                    <a href="{{url('blog-detail/'.$blog->id)}}">{{@$blog->title}}</a>
                                </h3>
                                <div class="description">
                                    <p>Proin viverra nisi at nisl imperdiet auctor. Donec ornare, est sed tincidunt placerat, sem mi suscipit mi.</p>
                                </div>    
                                <div class="bottom-content">
                                    <div class="thumb-author">
                                        <img src="{{@$blog->auhorimage}}" alt="">
                                       {{@$blog->author}}
                                    </div>
                                    <div class="date">{{@$blog->created_at}}</div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    @endforeach
                    <!-- Blog Slides -->
                    
                </div>
            </div>
        </section>
        <!-- Recent Blog Post End -->

        <!-- Pricing Plan Start -->
        
        <!-- Pricing Plan End -->

    </main>


@include('front.footer')