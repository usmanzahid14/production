@include('front.header')



   <!-- Page Breadcrumbs Start -->
    <section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page">
            <img class="ptt-png" src="{{url('images/Dot-Shape.png')}}" alt="png">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shopping Cart</li>
                    </ol>
                </nav>
                <h1>Shopping Cart</h1>
            </div>
        </div>
    </section>
    <!-- Page Breadcrumbs End -->

    <!-- Main Body Content Start -->
    <main id="body-content">

        <!-- Shopping Cart Start -->
        
        <section class="wide-tb-100 pb-0">
        	@if(count($cartItems) > 0)
            <div class="container">
                <form action="" method="post">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark theme-head">
                            <tr>
                                <th scope="col">&nbsp;</th>
                                <th scope="col"><span>Product</span></th>
                                <th scope="col"><span>Price</span></th>
                                <th scope="col"><span>Quantity</span></th>
                                <th scope="col"><span>Total</span></th>                                
                            </tr>
                        </thead>
                        <tbody class="theme-body">
                        	@foreach($cartItems as $row)
                             <?php $cart_image = $row['product']->getMedia('product_image'); ?>
                             
                            <tr>
                                <th scope="row">
                                    <a href="{{url('/delete-cart-item/'.$row['cart_id'])}}" class="link-oragne"><i data-feather="x-square"></i></a>
                                </th>
                                <td>
                                    <div class="item-product">
                                        <span class="img-wrap"><img src="{{ ($cart_image[0]->getUrl()) }}" alt=""></span>
                                        <span>{{@$row['product']->product_name}}</span>
                                    </div>
                                </td>
                                <td><strong class="txt-blue">{{@$row['product']->price}}</strong></td>
                                <td>
                                     <p>{{@$row['quantity']}}</p>
                                   <!--  <div class="quantity">
                                        <button class="minus-btn" type="button" name="button">
                                            <i data-feather="minus"></i>
                                        </button>   -->                                      
                                        <!-- <input type="text" name="name" value="{{@$row['quantity']}}"> -->

                                        <!-- <button class="plus-btn" type="button" name="button">
                                            <i data-feather="plus"></i>
                                        </button> 
                                    </div>-->
                                </td>
                                <td><strong class="txt-orange"><?php echo $row['product']->price * $row['quantity'];  ?></strong></td>
                            </tr>
                            @endforeach
                            
                           
                        </tbody>
                    </table>
                </div>
               <!--  <div class="row mt-4">
                    <div class="col-md-6">
                        
                    </div>
                    <div class="col-md-6">
                        <div class="text-md-right">
                        <button class="btn-theme bg-green btn-shadow" type="submit">Update cart</button>
                        </div>
                    </div>
                </div> -->
                </form>

                

                <div class="row mt-5 justify-content-md-end">
                    <div class="col-lg-4 col-md-6">
                        <div class="cart-totals">
                            <div class="px-4">
                                <div class="order-head">
                                    <span>Cart Totals</span>
                                </div>  
                            </div>
                            <div class="order-list">
                                <ul class="list-unstyled">
                                    
                                    <li>
                                        <span>Total</span>
                                        <span class="txt-green">{{$total}}</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="px-4">
                                <a href="{{url('/checkout')}}"><button class="btn-theme bg-orange btn-shadow btn-block" type="submit">Proceed to checkout</button></a>
                            </div>
                        </div>
                    </div>
                </div>

			@else
			<center>
			<h2>
			Your Cart Is Empty.
			</h2> 
		   </center>

			@endif
                
            </div>
            
        </section>

       

        <!-- Shopping Cart End -->

    </main>




@include('front.footer')
