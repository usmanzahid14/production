@include('front.header')

 <!-- Login Register Style CSSS -->
<link href="{{url('css/login-register.css')}}" rel="stylesheet">
@if(session('Already'))
<script type="text/javascript">
	alert('Email Alredy Registered');
</script>

@elseif(session('Invalid'))
<script type="text/javascript">
  alert('Invalid Credientials');
</script>

@endif

 
          
            @if(session('message'))
            <p class="alert alert-warning">
            {{session('message')}}</p>
            @endif
            <div class="container login-container" style="margin-top: 20%">
            <div class="row">
                <div class="col-md-6 login-form-1">
                    <h3>Login</h3>
                     <form action="{{ url('customers/signin') }}" method="post" >
                                 @csrf
                        <div class="form-group">
                              <input type="email" name="email"  class="form-control" placeholder="Email" required=""  />
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Your Password *" value="" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Login" />
                        </div>
                        
                    </form>
                    
                </div>
                <div class="col-md-6 login-form-2">
                    <div class="login-logo">
                        <img src="{{url('images/logo_dark.png')}}" alt=""/>
                    </div>
                      <form action="{{ url('customers/register') }}" method="post">
                      @csrf
                    <h3>Register</h3>
                        <div class="form-group">
                             <input type="text" name="name" class="form-control" placeholder="Name *" required="" />
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email *" required="" />
                        </div>

                          <div class="form-group">
                            <input type="Number" maxlength="10" minlength="10" class="form-control" name="phone" placeholder="Phone *" required="" />
                        </div>


                          <div class="form-group">
                             <input type="password" name="password" class="form-control" placeholder="Password *" value="" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Register" />
                        </div>
                         
                    </form>
                </div>
            </div>
        </div>

 
				
			 
		
		</div>
	</main>


@include('front.footer')
