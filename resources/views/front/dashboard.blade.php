@include('front.header')

<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #fecbca;
  width: 30%;
  height: 593px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #36829a;
  color: white;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #36829a;
  color: white;

}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
   width: 70%;
  border-left: none;
   
}
 
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #36829a;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}
 
</style>

    <!-- Page Breadcrumbs Start -->
    <section class="breadcrumbs-page-wrap" style="margin-top: 20%">        
        <div class="bg-navy-blue bg-fixed pos-rel breadcrumbs-page">
            <img class="ptt-png" src="{{url('images/Dot-Shape.png')}}" alt="png">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <h1>Dashboard</h1>
            </div>
        </div>
    </section>
    <!-- Page Breadcrumbs End -->

    <!-- Main Body Content Start -->
    <main id="body-content">
        <!-- Shopping Wide Start -->
        <section class="wide-tb-100 pb-0">

            <div class="container">
                 <div style="float: right;"><a class="btn btn-danger" href="{{url('customers/logout')}}" >Logout</a></div>
                 <div class="row mt-3 mb-5">
                    <h2> Wellcome   {{@$customer->name}}</h2>

                </div>
                    <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'Profile')" id="defaultOpen">Profile</button>
                    <button class="tablinks" onclick="openCity(event, 'Orders')">Orders</button>
                     </div>

                    <div id="Profile" class="tabcontent">
                    <h3>Profile</h3>
                         <form action="{{url('customers/updateCustomerProfile')}}" method="post">
                          @csrf
                          <input type="hidden" name="id" value="{{@$customer->id}}">
                        <label for="fname"> Name</label>
                        <input type="text" id="name" name="name" value="{{@$customer->name}}">

                        <label for="lname">Phone</label>
                        <input type="text" id="lname" name="phone" value="{{@$customer->phone}}" required="">


                         <label for="city">City</label>
                        <input type="text" id="lname" name="city" value="{{@$customer->city}}" required=""  >

                         <label for="address">Address</label>
                        <input type="text" id="lname" name="address" value="{{@$customer->address}}" >
 

                        <input type="submit" value="Update" class="mt-3">
                        </form>
                    </div>

                    <div id="Orders" class="tabcontent">
                    <h3>Orders</h3>
                    <p>Your Order History</p> 
                        <table>
                        <tr>
                        <th>Order #</th>
                        <th>Total Bill</th>
                        <th>Status</th>
                        <th>Order Date</th>
                        <th></th>


                        </tr>

 
                        @foreach($orderItems as $row)

                        <tr>
                        <td>{{@$row->ordernumber}}</td>
                          <td>{{@$row->total_bill}}</td>
                         <td>{{@$row->status}}</td>
                         <td>{{@$row->created_at}}</td>
                         <td><a class="btn btn-primary" id="edit" href="javascript:void(0);" data-id="{{@$row->ordernumber}}">More </a></td>





                        </tr>
                        @endforeach
                       
                        </table>
                    </div>
 
                 

                
                
            </div>
        </section>
        <!-- Shopping Wide End -->

    </main>


  <div class="modal fade" id="updateOrder" role="dialog">
    <div class="modal-dialog">    
    <!-- Modal content-->
    <div class="modal-content">

        <div class="modal-body">

        <h2 >Order # <span class="order"></span></h2>
        <br>
        <ul id="dataAppend">
        </ul>

        </div>
    </div>      
    </div>
  </div>  




   

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>

@include('front.footer')
 
<script type="text/javascript">

    $(document).ready(function(){

    $(document).on('click', '#edit', function(e){
   var data =$(this).data('id');
   //alert(data);
    $.ajax({
    method:"GET",
    data: data,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    modal: true,

    url:'{{url('admin/orders/edit')}}/'+data,

    success:function(data){
    $("#updateOrder").modal('show');
     
          $(function() {
            let html = "";
          $.each(data, function(i, data) {

          html += `
          <li>
          Product Name : ${data.product_name}
          </li>
          <li>
          Product Price: ${data.price}
          </li>
          <li>
          Quantity: ${data.quantity}
          </li>
          
          `
          //.appendTo('#records_table');
          // console.log($tr.wrap('<p>').html());
          });
          var $tr = $('#dataAppend').html(html);
          });

     //console.log(data);

    },
    error: function()
    {
    alert('Something Went Wrong. Please try again later. If the issue persists contact support');

    }
    });

    });
    });


    </script>
