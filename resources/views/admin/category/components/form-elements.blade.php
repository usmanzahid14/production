

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.category.columns.name') }}" required="">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('slug'), 'has-success': fields.slug && fields.slug.valid }">
    <label for="slug" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.slug') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.slug" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('slug'), 'form-control-success': fields.slug && fields.slug.valid}" id="slug" name="slug" placeholder="{{ trans('admin.category.columns.slug') }}">
        <div v-if="errors.has('slug')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('slug') }}</div>
    </div>
</div> -->

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('parent'), 'has-success': fields.parent && fields.parent.valid }">
    <label for="parent" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.parent') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

       <select name="parent" class="form-control"  v-model="form.parent" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('parent'), 'form-control-success': fields.parent && fields.parent.valid}" id="parent" required="">
           <option value="Scratch Post">Scratch Post</option>
           <option value="Houses">Houses</option>
           <option value="Cat Products">Cat Products</option>
           <option value="Dog Products" >Dog Products</option>      

       </select>


        <div v-if="errors.has('parent')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('parent') }}</div>
    </div>
</div>

<div id="hideshow" class="form-group row align-items-center" :class="{'has-danger': errors.has('subcategory'), 'has-success': fields.subcategory && fields.subcategory.valid }">
    <label for="subcategory" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.subcategory') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
         
         <select name="subcategory" class="form-control" id="appendCategory"  v-model="form.subcategory" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('subcategory'), 'form-control-success': fields.subcategory && fields.subcategory.valid}" id="subcategory">
             

         </select>  
        <div v-if="errors.has('subcategory')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('subcategory') }}</div>
    </div>
</div>






