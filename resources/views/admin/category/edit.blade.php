@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.category.actions.edit', ['name' => $category->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <category-form
                :action="'{{ $category->resource_url }}'"
                :data="{{ $category->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.category.actions.edit', ['name' => $category->name]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.category.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </category-form>

        </div>
    
</div>

@endsection


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
       $('#hideshow').hide();

    $('#parent').on('change', function() {
 //alert( 'this.value' ); // or $(this).val()
  if(this.value == "Cat Products") {
        $('#hideshow').show();  
        $('#appendCategory').html(`<option value="Food">Food</option>
        <option value="Litter & Wastage">Litter & Wastage</option>
        <option value="Accessories">Accessories</option>
        <option value="Grooming Supplies">Grooming Supplies</option>
        <option value="Veterinary">Veterinary</option>      
        <option value="Bath Shower">Bath Shower</option> `);
     
   }
    if(this.value == "Dog Products") {
    
        $('#hideshow').show();  
        $('#appendCategory').html(`<option value="Food">Food</option>
        <option value="Accessories">Accessories</option>
        <option value="Grooming Supplies">Grooming Supplies</option>
        <option value="Veterinary">Veterinary</option>      
        <option value="Bath Shower">Bath Shower</option> `);
     
   }
  if(this.value == "Houses") {
         $('#hideshow').hide();
       }
    if(this.value == "Scratch Post") {
         $('#hideshow').hide();
       }      
})
});

</script>