<!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('type'), 'has-success': fields.type && fields.type.valid }">
    <label for="type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.bank-detail.columns.type') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

        <select v-model="form.type" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('type'), 'form-control-success': fields.type && fields.type.valid}" id="type" name="type">
        <option  value="Jazz Cash">Jazz Cash</option>
        <option value="Easy Pasia">Easy Pasia</option>
        <option value="Bank Transfer">Bank Transfer</option>
    </select>
       
        <div v-if="errors.has('type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type') }}</div>
    </div>
</div> -->

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('accounttitle'), 'has-success': fields.accounttitle && fields.accounttitle.valid }">
    <label for="accounttitle" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.bank-detail.columns.accounttitle') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.accounttitle" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('accounttitle'), 'form-control-success': fields.accounttitle && fields.accounttitle.valid}" id="accounttitle" name="accounttitle" placeholder="{{ trans('admin.bank-detail.columns.accounttitle') }}">
        <div v-if="errors.has('accounttitle')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('accounttitle') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('accountnumber'), 'has-success': fields.accountnumber && fields.accountnumber.valid }">
    <label for="accountnumber" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.bank-detail.columns.accountnumber') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.accountnumber" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('accountnumber'), 'form-control-success': fields.accountnumber && fields.accountnumber.valid}" id="accountnumber" name="accountnumber" placeholder="{{ trans('admin.bank-detail.columns.accountnumber') }}">
        <div v-if="errors.has('accountnumber')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('accountnumber') }}</div>
    </div>
</div>


