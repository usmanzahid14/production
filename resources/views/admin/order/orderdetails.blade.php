@extends('brackets/admin-ui::admin.layout.default')
<style type="text/css">
.purchase
{
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 15px;
    font-family: Times New Roman;
}
.purchase header
{
    padding: 0px 0px 0px 0px;
    margin-bottom: 0px;
    border-bottom: 1px solid #3989c6;
}
.purchase header img
{
    max-width: 200px;
    margin-top: 0;
    margin-bottom: 0;
}
.purchase .company-details
{
    text-align: right;
    margin-top: 0;
    margin-bottom: 0;
}
.purchase main
{
    padding: 0px 0px;
    margin-bottom: 0px;
}
.purchase .to-details
{
    text-align: left;
}
.purchase .to-name
{
    font-weight: bold;
}
.purchase .to-name .to-address .to-city
{
    margin-top: 0;
    margin-bottom: 0;
}
.purchase .purchase-info
{
    text-align: right;
}
.purchase-info .info-code
{
    font-weight: bold;
}
.purchase-info .info-code .info-date
{
    margin-top: 0;
    margin-bottom: 0;
}
 table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2;
}	
</style>

@section('title', trans('admin.order.actions.index'))
@section('body')




<div>
    <div class="purchase overflow-auto">
        <!--<div style="min-width: 600px">-->
            <header>
                <div class="row">
            	    <div class="col-sm-3 col-xs-3">
                       <img src="{{url('images/logo_dark.png')}}"
                	     class="img-responsive">
                	</div>
                	 
            	</div>
            </header>
            <main>
                <div class="row  mt-2">
                    <div class="col-sm-3 col-xs-3 to-details">
                        <strong><p>ORDER#{{@$ordernumber}}</p></strong>
                        <div class="to-name">{{@$singleorder->name}}</div>
                        <div class="to-address">{{@$singleorder->address}},{{@$singleorder->city}}</div>
                        <div class="to-city">{{@$singleorder->phone}}</div>
                        <div class="to-city">{{@$singleorder->email}}</div>

                    </div>
                    <div class="col-sm-9 col-xs-9 purchase-info ">
                        <h4 class="info-code">{{@$singleorder->status}}</h4>
                        <div class="info-date">Order-Date : {{@$singleorder->created_at}}</div>
                    </div>

                </div>
                <div class="row" style="float: right !important; margin-right: 31px;">
                    <strong><h4><b>Total Bill :</b> {{$total_bill}}</h4></strong>

                </div>
                <div class="row mt-5">
                    <div class="col-sm-12 col-xs-12 table-responsive">
                        <table>
					<tr>
					<th>Product #</th>
					<th>Product Name</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Total</th>
                  

					 

					</tr>
					@foreach($orderItems as $row)
          
					<tr>
					<td>{{@$row->id}}</td>
					<td ><a href="{{url('admin/products/'.$row->product_id.'/edit')}}" target="a_blank">{{@$row->product_name}}</a></td>
					<td>{{@$row->quantity}}</td>
					<td>{{@$row->price}}</td>
					<td><?php echo $row->price * $row->quantity; ?></td>
					</tr>

					@endforeach
                    
						 
				</table>
                    </div>
                </div>
            </main>
        <!--</div>-->
    </div>
</div>













@endsection
