@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.order.actions.index'))

<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2;
}
#myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}
</style>
@section('body')
          <center>
           <h2 class="mt-5 mb-5">Pet Pat.pk Orders</h2>
           <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Order #.." title="Type in a order number">

           </center>
				<table id="myTable">
					<tr>
					<th>Order #</th>
					<th>Name</th>
					<th>Number</th>
					 
					<th>Date</th>
					<th>Payment Method</th>
					<th>Status</th>
					<th>Action</th>


					</tr>
					@foreach($data as $row)
						<tr>
						<td>{{@$row[0]->ordernumber}}</td>
						<td>{{@$row[0]->name}}</td>
						<td>{{@$row[0]->phone}}</td>
						<td>{{@$row[0]->created_at}}</td>
						<td>
							@if($row[0]->payment_method != 'Cash On Delivery')
							{{@$row[0]->payment_method}}<br>
							<a href="{{@$row[0]->bank_receipt}}" target="a_blank">View Reciept</a>
							@else
							{{@$row[0]->payment_method}}
							@endif

						</td>
						<td>{{@$row[0]->status}}</td>
						<td>
						<div class="row no-gutters">
						<div class="col-auto">
						<a class="btn btn-sm btn-spinner btn-info"  href="orders/details/{{$row[0]->ordernumber}}" title="Order Details" role="button">View Details</a>

						<a class="btn btn-sm  btn-danger"  id="edit" href="javascript:void(0);" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" data-id="{{$row[0]->ordernumber}}" role="button"><i class="fa fa-edit"></i></a>
						</div>
						 	 
						</div>
						</td>


						</tr>
						@endforeach
						 
				</table>


		<div class="modal fade" id="updateOrder" role="dialog">
		<div class="modal-dialog">    
		<!-- Modal content-->
		<div class="modal-content">

		<div class="modal-body">
		<center>  
          <form method="post" action="{{url('admin/orders/update')}}">
          	@csrf
          	<input class="ordernumber" name="ordernumber" type="hidden">
          	
          	<h2 >Order # <span class="order"></span></h2>
          	<br>
          	 <select class="status form-control mt-5" name="status">
			<option value="PROCESSING">PROCESSING</option>
			<option value="READY TO DELIVER">READY TO DELIVER </option>
			<option value="DELIVERED">DELIVERED</option>
			<option value="CANCELED">CANCELED</option>
			<option value="PENDING">PENDING</option>
			</select>

			<button type="submit" class="btn btn-primary mt-5 mb-5">Update</button>


          </form>
			
		 </center>
		<!--           <progress val="0" id="progress-bar"   style="display:none"></progress>
		-->        
		</div>
		</div>      
		</div>
		</div>  



	


@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<script>
	function myFunction() {
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("myInput");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
	td = tr[i].getElementsByTagName("td")[0];
	if (td) {
	txtValue = td.textContent || td.innerText;
	if (txtValue.toUpperCase().indexOf(filter) > -1) {
	tr[i].style.display = "";
	} else {
	tr[i].style.display = "none";
	}
	}       
	}
	}
	</script>


		<script type="text/javascript">

		$(document).ready(function(){

		$(document).on('click', '#edit', function(e){
		data =$(this).data('id');
		//alert(data);
		$.ajax({
		method:"GET",
		data: data,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		modal: true,

		url:'{{url('admin/orders/edit')}}/'+data,

		success:function(data){
		$("#updateOrder").modal('show');

		$('.status').val(data[0].status);
		$('.ordernumber').val(data[0].ordernumber);
		$('.order').text(data[0].ordernumber);

		 

		 console.log(data);

		},
		error: function()
		{
		alert('Something Went Wrong. Please try again later. If the issue persists contact support');

		}
		});

		});
		});


		</script>



