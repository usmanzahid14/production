<style type="text/css">
  .card-body{
    overflow: scroll;
  }
</style>
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.content') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users/dashboard') }}"><i class="nav-icon icon-flag"></i> Dashboard</a></li>

           <li class="nav-item"><a class="nav-link" href="{{ url('admin/roles') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.role.title') }}</a></li>
          
          <li class="nav-item"><a class="nav-link" href="{{ url('admin/customers') }}"><i class="nav-icon icon-globe"></i> {{ trans('admin.customer.title') }}</a></li>
          
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/contactus') }}"><i class="nav-icon icon-ghost"></i> {{ trans('admin.contactu.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/blogs') }}"><i class="nav-icon icon-book-open"></i> {{ trans('admin.blog.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/feedback') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.feedback.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/categories') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.category.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/products') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.product.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/orders') }}"><i class="nav-icon icon-drop"></i> {{ trans('admin.order.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/social-links') }}"><i class="nav-icon icon-plane"></i> {{ trans('admin.social-link.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/image-settings') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.image-setting.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/bank-details') }}"><i class="nav-icon icon-energy"></i> {{ trans('admin.bank-detail.title') }}</a></li>
           {{-- Do not delete me :) I'm used for auto-generation menu items --}}
            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.settings') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users') }}"><i class="nav-icon icon-user"></i> {{ __('Manage access') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i class="nav-icon icon-location-pin"></i> {{ __('Translations') }}</a></li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
