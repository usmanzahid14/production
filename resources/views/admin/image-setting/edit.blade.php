@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.image-setting.actions.edit', ['name' => $imageSetting->id]))

@section('body')

    <div class="container-xl">
        <div class="card">
            <h2 class="text-center">Update Image-settings</h2>   
                    <center>   <progress val="0" id="progress-bar"   style="display:none"></progress></center>

  
            <div class="myform form ">
               <form action="{{ $imageSetting->resource_url }}"  method="post" >
                @csrf
                <div class="row mt-5 ml-2">
                    <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image1') }}</label>
                         <img src="{{$imageSetting->image1}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image1" id="appendimage1" value="{{$imageSetting->image1}}">
                      <input type="file" id="image1" class="form-control">

                    </div>

                     <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image2') }}</label>
                         <img src="{{$imageSetting->image2}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image2" id="appendimage2" value="{{$imageSetting->image2}}">
                      <input type="file" id="image2" class="form-control">

                    </div>                   

                </div>


                 <div class="row mt-5 ml-2">
                     <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image3') }}</label>
                         <img src="{{$imageSetting->image2}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image3" id="appendimage3" value="{{$imageSetting->image3}}" >
                      <input type="file" id="image3" class="form-control">




                    </div>     
                          
                    <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image4') }}</label>
                         <img src="{{$imageSetting->image4}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image4" id="appendimage4" value="{{$imageSetting->image4}}" >
                      <input type="file" id="image4" class="form-control">


                    </div>                  

                </div>           
                   
                  
                    
                 <div class="row mt-3 ml-2">
                     <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image5') }}</label>
                         <img src="{{$imageSetting->image5}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image5" id="appendimage5" value="{{$imageSetting->image5}}">
                      <input type="file" id="image5" class="form-control">


                    </div>  

                   <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image6') }}</label>
                         <img src="{{$imageSetting->image6}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image6" id="appendimage6" value="{{$imageSetting->image6}}" >
                      <input type="file" id="image6" class="form-control">


                    </div>                     

                 </div>   

                <div class="row mt-3 ml-2">  
                 <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image7') }}</label>
                        <input type="text" name="image7" value="{{$imageSetting->image7}}" class="form-control my-input" id="title" placeholder="{{ trans('admin.image-setting.columns.image7') }}">
 

                    </div>

                    <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image8') }}</label>
                         <img src="{{$imageSetting->image8}}}"  height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image8" id="appendimage8" value="{{$imageSetting->image8}}" >
                      <input type="file" id="image8" class="form-control">


                    </div>  
                </div>

                <div class="row mt-3 ml-2">  
                 <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image9') }}</label>
                         <img src="{{$imageSetting->image9}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image9" id="appendimage9" value="{{$imageSetting->image9}}" >
                      <input type="file" id="image9" class="form-control">


                    </div>

                    <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image10') }}</label>
                         <img src="{{$imageSetting->image10}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image10" id="appendimage10" value="{{$imageSetting->image10}}">
                      <input type="file" id="image10" class="form-control">


                    </div>  
                </div>  

                 <div class="row mt-3 ml-2">  
                 <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image11') }}</label>
                         <img src="{{$imageSetting->image11}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image11" id="appendimage11" value="{{$imageSetting->image11}}" >
                      <input type="file" id="image11" class="form-control">


                    </div>

                    <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image12') }}</label>
                         <img src="{{$imageSetting->image12}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image12" id="appendimage12" value="{{$imageSetting->image12}}">
                      <input type="file" id="image12" class="form-control">


                    </div>  
                </div> 

                <div class="row mt-3 ml-2">  
                 <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image13') }}</label>
                         <img src="{{$imageSetting->image13}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image13" id="appendimage13" value="{{$imageSetting->image13}}" >
                      <input type="file" id="image13" class="form-control">


                    </div>

                    <div class="col-md-6">
                         <label >{{ trans('admin.image-setting.columns.image14') }}</label>
                         <img src="{{$imageSetting->image14}}}" height="40px" width="60px" class="ml-3 mt-3"> 
                      <input type="hidden" name="image14" id="appendimage14" value="{{$imageSetting->image14}}" >
                      <input type="file" id="image14" class="form-control">


                    </div>  
                </div>            

                  <div class="text-center mt-5 ">
                     <button type="submit" class=" btn btn-primary send-button tx-tfm">Publish Blog</button>
                  </div>


               </form>
            </div>
          

        </div>
    
</div>

<div class="modal fade" id="imageUpload" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
        <div class="modal-content">

        <div class="modal-body">
        <center>  <img src="https://cdn.dribbble.com/users/419257/screenshots/1724076/scanningwoohoo.gif" height="200px"> </center>
        <!--           <progress val="0" id="progress-bar"   style="display:none"></progress>
        -->        
        </div>
        </div>      
    </div>
  </div>  

@endsection




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-analytics.js"></script>
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-storage.js"></script>
    <script type="text/javascript">
const firebaseConfig = {
  apiKey: "AIzaSyCJixfHrcMJKK0ewT-FIbONJO1hwepQvn4",
  authDomain: "petpat-e9c67.firebaseapp.com",
  projectId: "petpat-e9c67",
  storageBucket: "petpat-e9c67.appspot.com",
  messagingSenderId: "576389095371",
  appId: "1:576389095371:web:b08c97c1292d780d72a04e",
  measurementId: "G-J4QB0LREH0"
};
 $(document).ready(function () {
   firebase.initializeApp(firebaseConfig);
   $("#image1").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage1").attr("data-imageUrl", downloadURL);
            $("#appendimage1").val(downloadURL)
          })
      }
   }),
   $("#image2").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage2").attr("data-imageUrl", downloadURL);
            $("#appendimage2").val(downloadURL)
          })
      }
   }),
   $("#image3").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage3").attr("data-imageUrl", downloadURL);
            $("#appendimage3").val(downloadURL)
          })
      }
   }),
   $("#image4").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage4").attr("data-imageUrl", downloadURL);
            $("#appendimage4").val(downloadURL)
          })
      }
   }),
   $("#image5").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage5").attr("data-imageUrl", downloadURL);
            $("#appendimage5").val(downloadURL)
          })
      }
   }),
   $("#image6").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage6").attr("data-imageUrl", downloadURL);
            $("#appendimage6").val(downloadURL)
          })
      }
   }),
   $("#image8").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage8").attr("data-imageUrl", downloadURL);
            $("#appendimage8").val(downloadURL)
          })
      }
   }),
   $("#image9").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage9").attr("data-imageUrl", downloadURL);
            $("#appendimage9").val(downloadURL)
          })
      }
   }),
   $("#image10").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage10").attr("data-imageUrl", downloadURL);
            $("#appendimage10").val(downloadURL)
          })
      }
   }),
   $("#image11").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage11").attr("data-imageUrl", downloadURL);
            $("#appendimage11").val(downloadURL)
          })
      }
   }),
   $("#image12").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage12").attr("data-imageUrl", downloadURL);
            $("#appendimage12").val(downloadURL)
          })
      }
   }),
   $("#image13").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage13").attr("data-imageUrl", downloadURL);
            $("#appendimage13").val(downloadURL)
          })
      }
   }),

    $("#image14").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage14").attr("data-imageUrl", downloadURL);
            $("#appendimage14").val(downloadURL)
          })
      }
   }),
     $("#image15").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendimage15").attr("data-imageUrl", downloadURL);
            $("#appendimage15").val(downloadURL)
          })
      }
   })
 

})
const uploadImageOnFirebase = function (file,) {
   return new Promise((resolve, reject) => {
      const fileExtension = file.name.split('.').slice(-1).pop();
      let filename = $.now() + Math.floor(Math.random() * 10000) + '.' + fileExtension;
      var storageRef = firebase.storage().ref('images/'+filename);
      var uploadTask = storageRef.put(file);
      uploadTask.on('state_changed',
         function progress(snapshot){
                // $(':input[type="button"]').prop('disabled', true);
               // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log(progress);
             // $('#uploader').val(progress);
            switch (snapshot.state) {
               case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
               break;
               case firebase.storage.TaskState.RUNNING: // or 'running'
                 // $('#imageUpload').modal('show');
                  $('#progress-bar').val(progress);
                  $('#progress-bar').css('display','inline');
                  //$('#imageUpload').modal('hide');


               break;
            }
            },
            function error(err){
            reject(err);
            },
         function complete() {
            uploadTask.snapshot.ref.getDownloadURL()
               .then(function (downloadURL) {
                $('#progress-bar').css('display','none');
                  resolve(downloadURL);
               })
            }

      )
   })
}
 </script>

 