@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.image-setting.actions.index'))

@section('body')

    <image-setting-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/image-settings') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.image-setting.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/image-settings/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.image-setting.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th></th>

                                        <th is='sortable' :column="'id'">{{ trans('admin.image-setting.columns.id') }}</th>
                                        <th is='sortable' :column="'image1'">{{ trans('admin.image-setting.columns.image1') }}</th>
                                        <th is='sortable' :column="'image2'">{{ trans('admin.image-setting.columns.image2') }}</th>
                                        <th is='sortable' :column="'image3'">{{ trans('admin.image-setting.columns.image3') }}</th>
                                        <th is='sortable' :column="'image4'">{{ trans('admin.image-setting.columns.image4') }}</th>
                                        <th is='sortable' :column="'image5'">{{ trans('admin.image-setting.columns.image5') }}</th>
                                        <th is='sortable' :column="'image6'">{{ trans('admin.image-setting.columns.image6') }}</th>
                                        <th is='sortable' :column="'image7'">{{ trans('admin.image-setting.columns.image7') }}</th>
                                        <th is='sortable' :column="'image8'">{{ trans('admin.image-setting.columns.image8') }}</th>
                                        <th is='sortable' :column="'image9'">{{ trans('admin.image-setting.columns.image9') }}</th>
                                        <th is='sortable' :column="'image10'">{{ trans('admin.image-setting.columns.image10') }}</th>
                                        <th is='sortable' :column="'image11'">{{ trans('admin.image-setting.columns.image11') }}</th>
                                        <th is='sortable' :column="'image12'">{{ trans('admin.image-setting.columns.image12') }}</th>
                                        <th is='sortable' :column="'image13'">{{ trans('admin.image-setting.columns.image13') }}</th>
                                        <th is='sortable' :column="'image14'">{{ trans('admin.image-setting.columns.image14') }}</th>
                                        <th is='sortable' :column="'image15'">{{ trans('admin.image-setting.columns.image15') }}</th>

                                        <th></th>
                                    </tr>
                                     
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>

                                    <td>@{{ item.id }}</td>
                                        <td> <img :src="item.image1" height="80px"></td>
                                        <td><img :src="item.image2" height="80px"></td>
                                        <td><img :src="item.image3" height="80px"></td>
                                        <td><img :src="item.image4" height="80px"></td>
                                        <td><img :src="item.image5" height="80px"></td>
                                        <td><img :src="item.image6" height="80px"></td>
                                        <td><img :src="item.image7" height="80px"></td>
                                        <td><img :src="item.image8" height="80px"></td>
                                        <td><img :src="item.image9" height="80px"></td>
                                        <td><img :src="item.image10" height="80px"></td>
                                        <td><img :src="item.image11" height="80px"></td>
                                        <td><img :src="item.image12" height="80px"></td>
                                        <td><img :src="item.image13" height="80px"></td>
                                        <td><img :src="item.image14" height="80px"></td>
                                        <td><img :src="item.image15" height="80px"></td>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/image-settings/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.image-setting.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </image-setting-listing>

@endsection