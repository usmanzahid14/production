<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image1'), 'has-success': fields.image1 && fields.image1.valid }">
    <label for="image1" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image1') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image1" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image1'), 'form-control-success': fields.image1 && fields.image1.valid}" id="image1" name="image1" placeholder="{{ trans('admin.image-setting.columns.image1') }}">
        <div v-if="errors.has('image1')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image1') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image2'), 'has-success': fields.image2 && fields.image2.valid }">
    <label for="image2" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image2') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image2" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image2'), 'form-control-success': fields.image2 && fields.image2.valid}" id="image2" name="image2" placeholder="{{ trans('admin.image-setting.columns.image2') }}">
        <div v-if="errors.has('image2')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image2') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image3'), 'has-success': fields.image3 && fields.image3.valid }">
    <label for="image3" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image3') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image3" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image3'), 'form-control-success': fields.image3 && fields.image3.valid}" id="image3" name="image3" placeholder="{{ trans('admin.image-setting.columns.image3') }}">
        <div v-if="errors.has('image3')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image3') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image4'), 'has-success': fields.image4 && fields.image4.valid }">
    <label for="image4" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image4') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image4" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image4'), 'form-control-success': fields.image4 && fields.image4.valid}" id="image4" name="image4" placeholder="{{ trans('admin.image-setting.columns.image4') }}">
        <div v-if="errors.has('image4')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image4') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image5'), 'has-success': fields.image5 && fields.image5.valid }">
    <label for="image5" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image5') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image5" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image5'), 'form-control-success': fields.image5 && fields.image5.valid}" id="image5" name="image5" placeholder="{{ trans('admin.image-setting.columns.image5') }}">
        <div v-if="errors.has('image5')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image5') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image6'), 'has-success': fields.image6 && fields.image6.valid }">
    <label for="image6" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image6') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image6" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image6'), 'form-control-success': fields.image6 && fields.image6.valid}" id="image6" name="image6" placeholder="{{ trans('admin.image-setting.columns.image6') }}">
        <div v-if="errors.has('image6')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image6') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image7'), 'has-success': fields.image7 && fields.image7.valid }">
    <label for="image7" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image7') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image7" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image7'), 'form-control-success': fields.image7 && fields.image7.valid}" id="image7" name="image7" placeholder="{{ trans('admin.image-setting.columns.image7') }}">
        <div v-if="errors.has('image7')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image7') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image8'), 'has-success': fields.image8 && fields.image8.valid }">
    <label for="image8" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image8') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image8" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image8'), 'form-control-success': fields.image8 && fields.image8.valid}" id="image8" name="image8" placeholder="{{ trans('admin.image-setting.columns.image8') }}">
        <div v-if="errors.has('image8')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image8') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image9'), 'has-success': fields.image9 && fields.image9.valid }">
    <label for="image9" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image9') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image9" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image9'), 'form-control-success': fields.image9 && fields.image9.valid}" id="image9" name="image9" placeholder="{{ trans('admin.image-setting.columns.image9') }}">
        <div v-if="errors.has('image9')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image9') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image10'), 'has-success': fields.image10 && fields.image10.valid }">
    <label for="image10" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image10') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image10" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image10'), 'form-control-success': fields.image10 && fields.image10.valid}" id="image10" name="image10" placeholder="{{ trans('admin.image-setting.columns.image10') }}">
        <div v-if="errors.has('image10')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image10') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image11'), 'has-success': fields.image11 && fields.image11.valid }">
    <label for="image11" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image11') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image11" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image11'), 'form-control-success': fields.image11 && fields.image11.valid}" id="image11" name="image11" placeholder="{{ trans('admin.image-setting.columns.image11') }}">
        <div v-if="errors.has('image11')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image11') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image12'), 'has-success': fields.image12 && fields.image12.valid }">
    <label for="image12" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image12') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image12" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image12'), 'form-control-success': fields.image12 && fields.image12.valid}" id="image12" name="image12" placeholder="{{ trans('admin.image-setting.columns.image12') }}">
        <div v-if="errors.has('image12')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image12') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image13'), 'has-success': fields.image13 && fields.image13.valid }">
    <label for="image13" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image13') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image13" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image13'), 'form-control-success': fields.image13 && fields.image13.valid}" id="image13" name="image13" placeholder="{{ trans('admin.image-setting.columns.image13') }}">
        <div v-if="errors.has('image13')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image13') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image14'), 'has-success': fields.image14 && fields.image14.valid }">
    <label for="image14" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image14') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image14" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image14'), 'form-control-success': fields.image14 && fields.image14.valid}" id="image14" name="image14" placeholder="{{ trans('admin.image-setting.columns.image14') }}">
        <div v-if="errors.has('image14')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image14') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image15'), 'has-success': fields.image15 && fields.image15.valid }">
    <label for="image15" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.image-setting.columns.image15') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image15" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image15'), 'form-control-success': fields.image15 && fields.image15.valid}" id="image15" name="image15" placeholder="{{ trans('admin.image-setting.columns.image15') }}">
        <div v-if="errors.has('image15')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image15') }}</div>
    </div>
</div>


