@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.product.actions.edit', ['name' => $product->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <product-form
                :action="'{{ $product->resource_url }}'"
                :data="{{ $product->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.product.actions.edit', ['name' => $product->id]) }}
                    </div>


                    <div class="card-body">
                        <div class="row ml-5 mb-5">

                           
                                @foreach ($product->getMedia('product_image') as $image)
                                 <center>
                                 <img src="{{ ($image->getUrl()) }}" height="200px" width="200px">
                               </center>
                                 @endforeach
                            
                        </div>
                        @include('admin.product.components.form-elements')
                         <div class="row  mt-5">
                            <div class="col-md-6">
                            <input type="checkbox" class="form-control"  name="reset" value="1" v-model="form.reset" v-validate="''" id="reset" >
                            <label for="reset"> Reset Previous Images</label><br>
                            </div>
                         </div>
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </product-form>

        </div>
    
</div>

@endsection