  
<!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('parent_category'), 'has-success': fields.parent_category && fields.parent_category.valid }">
    <label for="parent_category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.parent_category') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
         <select class="form-control" v-model="form.parent_category" v-validate="''" name="parent_category" id="parent_category">
             
                 <option value="Scratch Post">Scratch Post</option>
           <option value="Houses">Houses</option>
           <option value="Cat Products">Cat Products</option>
           <option value="Dog Products" >Dog Products</option>
         </select>  

      
        <div v-if="errors.has('parent_category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('parent_category') }}</div>
    </div>
</div> -->

<!-- <div id="hideshow" class="form-group row align-items-center" :class="{'has-danger': errors.has('sub_category'), 'has-success': fields.sub_category && fields.sub_category.valid }">
    <label for="sub_category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.sub_category') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

           <select name="sub_category" class="form-control"  v-model="form.sub_category" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('sub_category'), 'form-control-success': fields.sub_category && fields.sub_category.valid}" id="sub_category">
             

         </select>

 
        <div v-if="errors.has('sub_category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('sub_category') }}</div>
    </div>
</div> -->
<div class="row">
    <div class="col-md-6">
            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('category_id'), 'has-success': fields.category_id && fields.category_id.valid }">
            <label for="category_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.category_id') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

            <!-- <select class="form-control" v-model="form.category_id" v-validate="''" name="category_id" id="category_id">
            </select>  --> 
            <select class="form-control" v-model="form.category_id" v-validate="''" name="category_id" id="category_id">
            @foreach($category as $row)
            <option value="{{$row->id}}">&nbsp;&nbsp;&nbsp;{{$row->name}}&nbsp;&nbsp;&nbsp;From&nbsp;:&nbsp;&nbsp;{{$row->parent}}</option>
            @endforeach
            </select>




            <div v-if="errors.has('category_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('category_id') }}</div>
            </div>
            </div> 
    </div>

    <div class="col-md-6">
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_name'), 'has-success': fields.product_name && fields.product_name.valid }">
        <label for="product_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.product_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.product_name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('product_name'), 'form-control-success': fields.product_name && fields.product_name.valid}" id="product_name" name="product_name" placeholder="{{ trans('admin.product.columns.product_name') }}">
        <div v-if="errors.has('product_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_name') }}</div>
        </div>
        </div>    

    </div>

</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('price'), 'has-success': fields.price && fields.price.valid }">
        <label for="price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.price') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.price" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('price'), 'form-control-success': fields.price && fields.price.valid}" id="price" name="price" placeholder="{{ trans('admin.product.columns.price') }}">
        <div v-if="errors.has('price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('price') }}</div>
        </div>
        </div>

    </div>
     <div class="col-md-6">
          <div class="form-group row align-items-center" :class="{'has-danger': errors.has('firstprice'), 'has-success': fields.firstprice && fields.firstprice.valid }">
        <label for="firstprice" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">Price Before Discount</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.firstprice" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('firstprice'), 'form-control-success': fields.firstprice && fields.firstprice.valid}" id="firstprice" name="firstprice" >
        <div v-if="errors.has('firstprice')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('firstprice') }}</div>
        </div>
        </div>
      

    </div>

</div>

<div class="row">
    
  <div class="col-md-6" > 

    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('brand'), 'has-success': fields.brand && fields.brand.valid }">
    <label for="brand" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.brand') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
    <input type="text" v-model="form.brand" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('brand'), 'form-control-success': fields.brand && fields.brand.valid}" id="brand" name="brand" placeholder="{{ trans('admin.product.columns.brand') }}">
    <div v-if="errors.has('brand')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('brand') }}</div>
    </div>
    </div>


       
  </div>

        <div class="col-md-6" >
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('tag'), 'has-success': fields.tag && fields.tag.valid }">
        <label for="tag" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.tag') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">


        <select class="form-control" v-model="form.tag" v-validate="''" name="tag" id="tag">
        <option value="newarrival">New Arrival</option>
        <option value="catscratchers">Cat Scratchers</option>
        </select>     
        <div v-if="errors.has('tag')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tag') }}</div>
        </div>
        </div> 

        </div>


</div>
<div class="row">
    
  <div class="col-md-6" > 
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('size'), 'has-success': fields.size && fields.size.valid }">
        <label for="size" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.size') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.size" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('size'), 'form-control-success': fields.size && fields.size.valid}" id="size" name="size" placeholder="{{ trans('admin.product.columns.size') }}">
        <div v-if="errors.has('size')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('size') }}</div>
        </div>
        </div>
       
  </div>

        <div class="col-md-6" >
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('color'), 'has-success': fields.color && fields.color.valid }">
        <label for="color" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.color') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
          
        <input type="text" class="form-control" v-model="form.color" v-validate="''" name="color" id="color">
           
        <div v-if="errors.has('color')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('color') }}</div>
        </div>
        </div> 

        </div>


</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
        <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.product.columns.status') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <select v-model="form.status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('status'), 'form-control-success': fields.status && fields.status.valid}" id="status" name="status">

                <option value="Active">Active</option>
                <option value="Inactive">Inactive</option>
              
                </select>  
       
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
        </div>
        </div>
  </div>
   

</div>
<div class="row">

    <div class="col-md-12">
        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('description'), 'has-success': fields.description && fields.description.valid }">
        <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.description') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
        <wysiwyg v-model="form.description" v-validate="''" id="description" name="description" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description') }}</div>
        </div>
        </div>

    </div>

   

</div>
<div class="row">
    <div class="col-md-12">
        

        @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Product::class)->getMediaCollection('product_image'),
         'label' => 'Product Images'
        ])
    </div>   

</div>
  















