@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.product.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <product-form
            :action="'{{ url('admin/products') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.product.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.product.components.form-elements')
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </product-form>

        </div>

        </div>

    
@endsection


<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
       $('#hideshow').hide();

    $('#parent_category').on('change', function() {
 //alert( 'this.value' ); // or $(this).val()
 var parent_category = $(this).val();
//sessionStorage.setItem("parent_category",  parent_category);

  if(this.value == "Cat Products") {
        $('#hideshow').show();  
        $('#sub_category').html(`<option value="Food">Food</option>
        <option value="Litter & Wastage">Litter & Wastage</option>
        <option value="Accessories">Accessories</option>
        <option value="Grooming Supplies">Grooming Supplies</option>
        <option value="Veterinary">Veterinary</option>      
        <option value="Bath Shower">Bath Shower</option> `);
     
   }
    if(this.value == "Dog Products") {
    
        $('#hideshow').show();  
        $('#sub_category').html(`<option value="Food">Food</option>
        <option value="Accessories">Accessories</option>
        <option value="Grooming Supplies">Grooming Supplies</option>
        <option value="Veterinary">Veterinary</option>      
        <option value="Bath Shower">Bath Shower</option> `);
     
   }
  if(this.value == "Houses") {
         $('#hideshow').hide();
       }
    if(this.value == "Scratch Post") {
         $('#hideshow').hide();
       }      
})

 $('#sub_category').on('change', function() {
 var sub_category = $(this).val();
 var parent = document.getElementById("parent_category").value;
 if(parent == '')
 //alert(parent_category);

 //var parent=sessionStorage.getItem("parent_category");

 //alert(parent); // or $(this).val()
 //alert(sub_category); // or $(this).val()

let endpoint = 'http://127.0.0.1:8000/admin/categories';
let apiKey = 'get-from-ajax-request';

       $.ajax({
            url: endpoint + "/" + apiKey + "/" +parent + "/" + sub_category,
            type:"get",
            cache:'false',
            contentType: "application/json",
            dataType: 'json',
            
            success: function(data){
                  //console.log(data);

                    $(function() {
                    $.each(data, function(i, data) {
                         
                      var $tr = $('#category_id').append(`<option value="${data.id}">${data.name}</option>`);

                    });
                });
               
            },
            error: function() {
                alert('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.');
            }
        });

  


})
});

</script> -->
