@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.blog.actions.create'))

@section('body')

    
            <div class="container">
            <div class="col-md-6 mx-auto text-center">
            <div class="header-title">
            <h1 class="wv-heading--title">
            New Blog
            <progress val="0" id="progress-bar"   style="display:none"></progress>
            </h1>

            </div>
            </div>
      
                   

            <div class="myform form ">
               <form action="{{ url('admin/blogs/') }}"  method="post" >
                @csrf
                <div class="row mt-5">
                    <div class="col-md-6">
                         <label >Title</label>
                      <input type="text" name="title"  class="form-control my-input" id="title" placeholder="Title">

                    </div>

                     <div class="col-md-6">
                        <label >Tag</label> 
                      <input type="text" min="0" name="tag" id="tag"  class="form-control my-input" placeholder="tag">
                      </div>                   

                </div>


                 <div class="row mt-5">
                    <div class="col-md-6">
                    <label >Category</label>

                     <input type="text" min="0" name="category" id="category"  class="form-control my-input" placeholder="category">
                    </div>
                          
                     <div class="col-md-6"> 
                    <label >Author Name</label>

                     <input type="text" min="0" name="authorname" id="authorname"  class="form-control my-input" placeholder="authorname">
                    </div>                  

                </div>           
                   
                  
                    
                 <div class="row mt-3">
                    <div class="col-md-6">
                    <input type="hidden"  class="blogimage form-control" id="appendblogimage" name="blogimage">

                    <label >Blog Image</label>
                    <div class="form-group">
                    <input type="file" class="form-control" multiple="multiple"  id="blogimage"/>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <input type="hidden" min="0" name="authorimage" id="appendauthorimage"  class="form-control my-input" >
                    <label >Author Profile Image</label>
                    <div class="form-group">
                    <input type="file" class="form-control" multiple="multiple"  id="authorimage"/>
                    </div>
                    </div>                    

                 </div>   

                <div class="row mt-3">  
                <label >Description</label>
                <div class="form-group">                  
                <div class="col-md-12">                      
                <wysiwyg   id="description" name="description" config="mediaWysiwygConfig"></wysiwyg>
                </div>
                </div>
                </div>            

                  <div class="text-center ">
                     <button type="submit" class=" btn btn-primary send-button tx-tfm">Publish Blog</button>
                  </div>


               </form>
            </div>
        
   </div>

  <div class="modal fade" id="imageUpload" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
        <div class="modal-content">

        <div class="modal-body">
        <center>  <img src="https://cdn.dribbble.com/users/419257/screenshots/1724076/scanningwoohoo.gif" height="200px"> </center>
        <!--           <progress val="0" id="progress-bar"   style="display:none"></progress>
        -->        
        </div>
        </div>      
    </div>
  </div>  

    
@endsection



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-analytics.js"></script>
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-storage.js"></script>
    <script type="text/javascript">
const firebaseConfig = {
  apiKey: "AIzaSyCJixfHrcMJKK0ewT-FIbONJO1hwepQvn4",
  authDomain: "petpat-e9c67.firebaseapp.com",
  projectId: "petpat-e9c67",
  storageBucket: "petpat-e9c67.appspot.com",
  messagingSenderId: "576389095371",
  appId: "1:576389095371:web:b08c97c1292d780d72a04e",
  measurementId: "G-J4QB0LREH0"
};
 $(document).ready(function () {
   firebase.initializeApp(firebaseConfig);
   $("#blogimage").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendblogimage").attr("data-imageUrl", downloadURL);
            $("#appendblogimage").val(downloadURL)
          })
      }
   }),
   $("#authorimage").on("change", e => {
      console.log($(".image").val());
      console.log(e.target.files);
      const files = e.target.files;
      for(let i = 0; i < files.length; i++) {
        uploadImageOnFirebase(files[i])
          .then(downloadURL => {


            // document.querySelector(".image").value = downloadURL;
            $("#appendauthorimage").attr("data-imageUrl", downloadURL);
            $("#appendauthorimage").val(downloadURL)
          })
      }
   })

})
const uploadImageOnFirebase = function (file,) {
   return new Promise((resolve, reject) => {
      const fileExtension = file.name.split('.').slice(-1).pop();
      let filename = $.now() + Math.floor(Math.random() * 10000) + '.' + fileExtension;
      var storageRef = firebase.storage().ref('images/'+filename);
      var uploadTask = storageRef.put(file);
      uploadTask.on('state_changed',
         function progress(snapshot){
                // $(':input[type="button"]').prop('disabled', true);
               // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log(progress);
             // $('#uploader').val(progress);
            switch (snapshot.state) {
               case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
               break;
               case firebase.storage.TaskState.RUNNING: // or 'running'
                  //$('#imageUpload').modal('show');
                  $('#progress-bar').val(progress);
                  $('#progress-bar').css('display','inline');
                  //$('#imageUpload').modal('hide');


               break;
            }
            },
            function error(err){
            reject(err);
            },
         function complete() {
            uploadTask.snapshot.ref.getDownloadURL()
               .then(function (downloadURL) {
                $('#progress-bar').css('display','none');
                  resolve(downloadURL);
               })
            }

      )
   })
}
 </script>
