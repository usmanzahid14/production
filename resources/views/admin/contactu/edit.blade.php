@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.contactu.actions.edit', ['name' => $contactu->email]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <contactu-form
                :action="'{{ $contactu->resource_url }}'"
                :data="{{ $contactu->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.contactu.actions.edit', ['name' => $contactu->email]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.contactu.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </contactu-form>

        </div>
    
</div>

@endsection