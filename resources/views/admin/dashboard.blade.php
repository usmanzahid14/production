@extends('brackets/admin-ui::admin.layout.default')

@section('title', 'Dashboard')
<style type="text/css">
	  <style>
        .row {
            display: flex;
            justify-content: space-around;
        }

        .second_row {
            margin-top: 1rem;
        }
        .third_row {
            margin-top: 1rem;
        }

        .rider {
            display: flex;
            flex-direction: column;
            background-color: white;
            text-align: center;
            width: 130px;
            height: 170px;
            box-shadow: 5px 5px 2px 2px #888888;
            border-radius: 20px;
        }

        .rider img {
            width: 80px;
            height: 80px;
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 20px;
        }

        .rider span {
            margin-top: 10px;
            margin-bottom: 5px;
            font-weight: bold;
            font-size: 20px;
        }
        .container {
            margin-top: 100px
        }

        .section-title {
            margin-bottom: 38px
        }

        .shadow,
        .subscription-wrapper {
            box-shadow: 0px 15px 39px 0px rgba(8, 18, 109, 0.1) !important
        }

        .icon-primary {
            color: #062caf
        }

        .icon-bg-circle {
            position: relative
        }

        .icon-lg {
            font-size: -webkit-xxx-large;
        }

        .icon-bg-circle::before {
            z-index: 1;
            position: relative
        }

        .icon-bg-primary::after {
            background: #062caf !important
        }

        .icon-bg-circle::after {
            content: '';
            position: absolute;
            width: 68px;
            height: 68px;
            top: -35px;
            left: 15px;
            border-radius: 50%;
            background: inherit;
            opacity: .1
        }

        p,
        .paragraph {
            font-weight: 400;
            color: #8b8e93;
            font-size: 15px;
            line-height: 1.6;
            font-family: "Open Sans", sans-serif
        }

        .icon-bg-yellow::after {
            background: #f6a622 !important
        }

        .icon-bg-purple::after {
            background: #7952f5
        }

        .icon-yellow {
            color: #f6a622
        }

        .icon-purple {
            color: #7952f5
        }

        .icon-cyan {
            color: #02d0a1
        }

        .icon-bg-cyan::after {
            background: #02d0a1
        }

        .icon-bg-red::after {
            background: #ff4949
        }

        .icon-red {
            color: #ff4949
        }

        .icon-bg-green::after {
            background: #66cc33
        }

        .icon-green {
            color: #66cc33
        }

        .icon-bg-orange::after {
            background: #ff7c17
        }

        .icon-orange {
            color: #ff7c17
        }

        .icon-bg-blue::after {
            background: #3682ff
        }

        .icon-blue {
            color: #3682ff
        }
        .fa-users::before{
            font-size: 50px;
        }
        .div-link{
            cursor: pointer;
            text-decoration: none;
            color: #23282c;
        }
        .div-link:hover{
            cursor: pointer;
            text-decoration: none;
            color: #23282c;
        }
    </style>
</style>
@section('body')


 <div class="container" style="margin-top: 50px !important;">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-title"> DASHBOARD</h2>
            </div>

            <div class="col-lg-3 col-sm-6 mb-4">
                <a href="{{ url('admin/customers') }}" class="div-link">
                    <div class="card border-0 shadow rounded-xs pt-5">
                        <div class="" style="text-align-last: center;"> <i class="fa fa-users icon-lg icon-primary icon-bg-primary icon-bg-circle mb-3"></i>
                            <h4 class="mt-4 mb-3">Active Customers <br> {{$customer}}</h4>
                        </div>

                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 mb-4">
                <a href="{{ url('admin/products') }}" class="div-link">
                    <div class="card border-0 shadow rounded-xs pt-5">
                        <div class="" style="text-align-last: center;"> <i class="fa fa-users icon-lg icon-yellow icon-bg-yellow icon-bg-circle mb-3"></i>
                            <h4 class="mt-4 mb-3">Active Products <br>{{$product}}</h4>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6 mb-4">
                <a href="{{ url('admin/contactus/') }}" class="div-link">
                    <div class="card border-0 shadow rounded-xs pt-5">
                        <div class="" style="text-align-last: center;"> <i class="fa fa-users icon-lg icon-purple icon-bg-purple icon-bg-circle mb-3"></i>
                            <h4 class="mt-4 mb-3">Contact Us<br> {{$contactus}}</h4>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6 mb-4">
                <a href="{{ url('admin/blogs') }}" class="div-link">
                    <div class="card border-0 shadow rounded-xs pt-5">
                        <div class="" style="text-align-last: center;"> <i class="fa fa-users icon-lg icon-cyan icon-bg-cyan icon-bg-circle mb-3"></i>
                            <h4 class="mt-4 mb-3">Blogs<br>{{$blog}}</h4>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>



 <div class="container" >
         <div class="row">
            <div class="col-md-12">
                <div id="salesMonthlyChart" style="height: 350px; width: 100%;"></div>
            </div>
         </div>  
        <div class="row">
            <div class="col-md-6">
                <div id="orderStatus" style="height: 350px; width: 100%;"></div>
            </div>
         
            <div class="col-md-6">
                <div id="paymentGraph" style="height: 350px; width: 100%;"></div>
            </div>
        </div>
</div>





@endsection

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
window.onload = function () {

var orderStatus = new CanvasJS.Chart("orderStatus", {
    animationEnabled: true,
    theme: "light1", // "light1", "light2", "dark1", "dark2"
    title:{
        text: "Orders Tracking"
    },
    axisY: {
        title: "orders"
    },
    data: [{        
        type: "column",  
        showInLegend: true, 
        legendMarkerColor: "grey",
        legendText: "MMbbl = one million barrels",
        dataPoints: <?php echo json_encode($orderStatus, JSON_NUMERIC_CHECK); ?>
    }]
});
orderStatus.render();


var paymentGraph = new CanvasJS.Chart("paymentGraph", {
    exportEnabled: true,
    animationEnabled: true,
    title:{
        text: "Payment Method Ratio of Orders"
    },
    legend:{
        cursor: "pointer",
    },
    data: [{
        type: "pie",
        showInLegend: true,
        indexLabel: "{name} - {y}-orders",
        dataPoints:<?php echo json_encode($orderpayment, JSON_NUMERIC_CHECK); ?>
    }]
});
paymentGraph.render();

var salesMonthlyChart = new CanvasJS.Chart("salesMonthlyChart", {
            title:{
                text: "Monthly Sales Report"
            },
            axisX:{
                interval: 1,
                intervalType: "month"
            },
            data: [
                {
                    type: "line",
                    dataPoints:<?php echo json_encode($dataPointsSaleMonthly,
                        JSON_NUMERIC_CHECK); ?>
                }
            ]
        });
        salesMonthlyChart.render();

}
</script>
